﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace System.Csv
{
    public class CsvWriter
    {
        public const string Seperator = ",";

        public Stream BaseStream { get; private set; }

        public CsvWriter(Stream baseStream)
        {
            if (baseStream == null)
                throw new ArgumentNullException("baseStream");

            BaseStream = baseStream;
        }

        public void WriteLine(string[] values)
        {
            StringBuilder sb = new StringBuilder();

            foreach (var item in values)
            {
                string s = item ?? "";

                if (HasSpecialCharacters(s))
                {
                    s = ReplaceSpecialCharacters(item);
                }

                if (sb.Length == 0)
                    sb.Append(s);
                else
                    sb.Append(CsvWriter.Seperator + s);
            }

            sb.AppendLine();


            byte[] bytes = Encoding.Default.GetBytes(sb.ToString());
            BaseStream.Write(bytes, 0, bytes.Length);
        }

        private bool HasSpecialCharacters(string value)
        {
            return value.IndexOfAny(new char[] { ',', '\"' }) != -1;
        }

        private string ReplaceSpecialCharacters(string value)
        {
            value = value.Replace("\"", "\"\"");
            value = '\"' + value + '\"';
            return value;
        }
    }
}
