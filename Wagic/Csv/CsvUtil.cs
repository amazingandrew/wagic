﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Csv;
using System.IO;
using System.Reflection;

namespace System.Csv
{
    public class CsvUtil
    {
        public static string[] GetValues(object obj)
        {
            var fields = CsvUtil.GetFields(obj.GetType());

            List<string> list = new List<string>();
            foreach (var f in fields)
            {
                string value = f.Key.GetValue(obj, null) as string;
                list.Add(value);
            }
            return list.ToArray();
        }
        public static string[] GetHeaderRecords(object obj)
        {
            var fields = CsvUtil.GetFields(obj.GetType()).Values;
            return fields.Select(x => x.FieldName).ToArray();
        }

        public static void WriteFile<T>(IEnumerable<T> records, string filePath)
        {
            //open file
            using (Stream stream = File.Open(filePath, FileMode.Create, FileAccess.ReadWrite))
            {
                //write
                CsvWriter writer = new CsvWriter(stream);
                WriteToCsv<T>(records, writer);
                stream.Close();
            }
        }
        public static void WriteToCsv<T>(IEnumerable<T> records, CsvWriter writer)
        {
            //get header record
            T headerReference = records.FirstOrDefault();
            if (headerReference == null)
                headerReference = (T)Activator.CreateInstance(typeof(T));

            //write header
            string[] header = CsvUtil.GetHeaderRecords(headerReference);
            writer.WriteLine(header);

            //write values
            foreach (var item in records)
            {
                writer.WriteLine(CsvUtil.GetValues(item));
            }
        }

        public static List<T> ReadFile<T>(string filePath)
        {
            CsvReader reader = CsvReader.Open(filePath);
            return ReadFromCsv<T>(reader);
        }
        public static List<T> ReadFromCsv<T>(CsvReader reader)
        {
            List<T> list = new List<T>();

            while (reader.Read())
            {
                T record = Activator.CreateInstance<T>();
                CsvUtil.GetRecord(reader, record);
                list.Add((T)record);
            }

            return list;
        }

        #region Helper Methods

        private static CsvFieldAttribute Get(PropertyInfo property)
        {
            var attr = property.GetCustomAttributes(typeof(CsvFieldAttribute), true)
                .OfType<CsvFieldAttribute>().FirstOrDefault();

            return attr;
        }
        private static Dictionary<PropertyInfo, CsvFieldAttribute> GetFields(Type t)
        {
            Dictionary<PropertyInfo, CsvFieldAttribute> result = new Dictionary<PropertyInfo, CsvFieldAttribute>();

            PropertyInfo[] properties = t.GetProperties();
            List<CsvFieldAttribute> fields = new List<CsvFieldAttribute>();

            foreach (var p in properties)
            {
                CsvFieldAttribute a = CsvUtil.Get(p);
                if (a != null)
                    result.Add(p, a);
            }

            return result;
        }
        private static void GetRecord(CsvReader reader, object obj)
        {
            var fields = CsvUtil.GetFields(obj.GetType());

            foreach (var f in fields)
            {
                int ordinal = reader.GetOrdinal(f.Value.FieldName);

                if (ordinal == -1) //field not found
                {
                    if (f.Value.Required == CsvRequiredField.Required)
                        throw new CsvException(string.Format("Required field {0} was not found in line {1}",
                            f.Value.FieldName, reader.NumberOfLinesRead));
                    else
                        continue;
                }
                else
                {
                    //get string
                    string csvValue = reader.GetString(ordinal);

                    //set in class
                    f.Key.SetValue(obj, csvValue, null);
                }
            }
        }

        #endregion
    }
}
