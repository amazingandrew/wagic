﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace System.Csv
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class CsvFieldAttribute : Attribute
    {
        public string FieldName { get; set; }
        public CsvRequiredField Required { get; set; }

        public CsvFieldAttribute(string fieldName)
        {
            if (fieldName == null)
                throw new ArgumentNullException("fieldName");

            this.FieldName = fieldName;
            this.Required = CsvRequiredField.NotRequired;
        }
        public CsvFieldAttribute(string fieldName, CsvRequiredField required)
        {
            if (fieldName == null)
                throw new ArgumentNullException("fieldName");

            this.FieldName = fieldName;
            this.Required = required;
        }
    }

    public enum CsvRequiredField
    {
        NotRequired,
        Required
    }
}
