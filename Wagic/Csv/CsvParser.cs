﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace System.Csv
{
    /// <summary>
    /// This just implements splitting a single line into fields.
    /// This item was ported from the Java OpenCSV library.
    /// http://opencsv.sourceforge.net
    /// </summary>
    public class CsvParser
    {
        #region Defaults

        /// <summary>
        /// The default separator to use if none is supplied to the constructor.
        /// </summary>
        public static readonly char DEFAULT_SEPARATOR = ',';

        public static readonly int INITIAL_READ_SIZE = 128;

        /// <summary>
        /// The default quote character to use if none is supplied to the constructor.
        /// </summary>
        public static readonly char DEFAULT_QUOTE_CHARACTER = '"';


        /// <summary>
        /// The default escape character to use if none is supplied to the constructor.
        /// </summary>
        public static readonly char DEFAULT_ESCAPE_CHARACTER = '\\';

        /// <summary>
        /// The default strict quote behavior to use if none is supplied to the constructor
        /// </summary>
        public static readonly bool DEFAULT_STRICT_QUOTES = false;

        /// <summary>
        /// The default leading whitespace behavior to use if none is supplied to the constructor
        /// </summary>
        public static readonly bool DEFAULT_IGNORE_LEADING_WHITESPACE = true;

        #endregion

        private readonly char _separator;
        private readonly char _quotechar;
        private readonly char _escape;
        private readonly bool _strictQuotes;

        private string pending;
        private bool inField = false;

        private readonly bool ignoreLeadingWhiteSpace;

        #region Constructors

        /// <summary>
        /// Constructs CSVParser using a comma for the separator.
        /// </summary>
        public CsvParser()
            : this(DEFAULT_SEPARATOR, DEFAULT_QUOTE_CHARACTER, DEFAULT_ESCAPE_CHARACTER)
        {

        }

        /// <summary>
        /// Constructs CSVParser with supplied separator. 
        /// </summary>
        /// <param name="separator">the delimiter to use for separating entries.</param>
        public CsvParser(char separator)
            : this(separator, DEFAULT_QUOTE_CHARACTER, DEFAULT_ESCAPE_CHARACTER)
        {

        }

        /// <summary>
        /// Constructs CSVParser with supplied separator and quote char.
        /// </summary>
        /// <param name="separator">the delimiter to use for separating entries.</param>
        /// <param name="quotechar">the character to use for quoted elements</param>
        public CsvParser(char separator, char quotechar)
            : this(separator, quotechar, DEFAULT_ESCAPE_CHARACTER)
        {

        }

        /// <summary>
        /// Constructs CSVReader with supplied separator and quote char.
        /// </summary>
        /// <param name="separator">the delimiter to use for separating entries</param>
        /// <param name="quotechar">the character to use for quoted elements</param>
        /// <param name="escape">the character to use for escaping a separator or quote</param>
        public CsvParser(char separator, char quotechar, char escape)
            : this(separator, quotechar, escape, DEFAULT_STRICT_QUOTES)
        {

        }

        /// <summary>
        /// Constructs CSVReader with supplied separator and quote char.
        /// Allows setting the "strict quotes" flag
        /// </summary>
        /// <param name="separator">the delimiter to use for separating entries</param>
        /// <param name="quotechar">the character to use for quoted elements</param>
        /// <param name="escape">the character to use for escaping a separator or quote</param>
        /// <param name="strictQuotes">if true, characters outside the quotes are ignored</param>
        public CsvParser(char separator, char quotechar, char escape, bool strictQuotes)
            : this(separator, quotechar, escape, strictQuotes, DEFAULT_IGNORE_LEADING_WHITESPACE)
        {

        }

        /// <summary>
        /// Constructs CSVReader with supplied separator and quote char.
        /// Allows setting the "strict quotes" and "ignore leading whitespace" flags
        /// </summary>
        /// <param name="separator">the delimiter to use for separating entries</param>
        /// <param name="quotechar">the character to use for quoted elements</param>
        /// <param name="escape">the character to use for escaping a separator or quote</param>
        /// <param name="strictQuotes">if true, characters outside the quotes are ignored</param>
        /// <param name="ignoreLeadingWhiteSpace">if true, white space in front of a quote in a field is ignored</param>
        public CsvParser(char separator, char quotechar, char escape, bool strictQuotes, bool ignoreLeadingWhiteSpace)
        {
            this._separator = separator;
            this._quotechar = quotechar;
            this._escape = escape;
            this._strictQuotes = strictQuotes;
            this.ignoreLeadingWhiteSpace = ignoreLeadingWhiteSpace;
        }

        #endregion

        /// <summary>
        /// Returns true if something was left over from last call(s)
        /// </summary>
        /// <returns>Returns true if something was left over from last call(s)</returns>
        public bool IsPending()
        {
            return pending != null;
        }

        public string[] ParseLineMulti(string nextLine)
        {
            return ParseLine(nextLine, true);
        }

        public string[] ParseLine(string nextLine)
        {
            return ParseLine(nextLine, false);
        }

        /// <summary>
        /// Parses an incoming string and returns an array of elements.
        /// </summary>
        /// <param name="nextLine">the string to parse</param>
        /// <param name="multi">is multi line</param>
        /// <returns>the comma-tokenized list of elements, or null if nextLine is null</returns>
        private string[] ParseLine(string nextLine, bool multi)
        {

            if (!multi && pending != null)
            {
                pending = null;
            }

            if (nextLine == null)
            {
                if (pending != null)
                {
                    string s = pending;
                    pending = null;
                    return new string[] { s };
                }
                else
                {
                    return null;
                }
            }

            List<string> tokensOnThisLine = new List<string>();
            StringBuilder sb = new StringBuilder(INITIAL_READ_SIZE);
            bool inQuotes = false;
            if (pending != null)
            {
                sb.Append(pending);
                pending = null;
                inQuotes = true;
            }
            for (int i = 0; i < nextLine.Length; i++)
            {

                char c = nextLine[i];
                if (c == this._escape)
                {
                    if (IsNextCharacterEscapable(nextLine, inQuotes || inField, i))
                    {
                        sb.Append(nextLine[i + 1]);
                        i++;
                    }
                }
                else if (c == _quotechar)
                {
                    if (IsNextCharacterEscapedQuote(nextLine, inQuotes || inField, i))
                    {
                        sb.Append(nextLine[i + 1]);
                        i++;
                    }
                    else
                    {
                        inQuotes = !inQuotes;

                        // the tricky case of an embedded quote in the middle: a,bc"d"ef,g
                        if (!_strictQuotes)
                        {
                            if (i > 2 //not on the beginning of the line
                                    && nextLine[i - 1] != this._separator //not at the beginning of an escape sequence
                                    && nextLine.Length > (i + 1) &&
                                    nextLine[i + 1] != this._separator //not at the	end of an escape sequence
                            )
                            {

                                if (ignoreLeadingWhiteSpace && sb.Length > 0 && IsAllWhiteSpace(sb))
                                {
                                    sb = new StringBuilder(INITIAL_READ_SIZE);  //discard white space leading up to quote
                                }
                                else
                                {
                                    sb.Append(c);
                                }

                            }
                        }
                    }
                    inField = !inField;
                }
                else if (c == _separator && !inQuotes)
                {
                    tokensOnThisLine.Add(sb.ToString());
                    sb = new StringBuilder(INITIAL_READ_SIZE); // start work on next token
                    inField = false;
                }
                else
                {
                    if (!_strictQuotes || inQuotes)
                    {
                        sb.Append(c);
                        inField = true;
                    }
                }
            }
            // line is done - check status
            if (inQuotes)
            {
                if (multi)
                {
                    // continuing a quoted section, re-append newline
                    sb.Append("\n");
                    pending = sb.ToString();
                    sb = null; // this partial content is not to be added to field list yet
                }
                else
                {
                    throw new IOException("Un-terminated quoted field at end of CSV line");
                }
            }
            if (sb != null)
            {
                tokensOnThisLine.Add(sb.ToString());
            }

            return tokensOnThisLine.ToArray();
        }

        /// <summary>
        /// precondition: the current character is a quote or an escape
        /// </summary>
        /// <param name="nextLine">nextLine the current line</param>
        /// <param name="inQuotes">inQuotes true if the current context is quoted</param>
        /// <param name="i">i current index in line</param>
        /// <returns>true if the following character is a quote</returns>
        private bool IsNextCharacterEscapedQuote(string nextLine, bool inQuotes, int i)
        {
            return inQuotes  // we are in quotes, therefore there can be escaped quotes in here.
                && nextLine.Length > (i + 1)  // there is indeed another character to check.
                && nextLine[i + 1] == _quotechar;
        }

        /// <summary>
        /// precondition: the current character is an escape
        /// </summary>
        /// <param name="nextLine">nextLine the current line</param>
        /// <param name="inQuotes">inQuotes true if the current context is quoted</param>
        /// <param name="i">i current index in line</param>
        /// <returns>true if the following character is a quote</returns>
        protected bool IsNextCharacterEscapable(string nextLine, bool inQuotes, int i)
        {
            return inQuotes  // we are in quotes, therefore there can be escaped quotes in here.
                && nextLine.Length > (i + 1)  // there is indeed another character to check.
                && (nextLine[i + 1] == _quotechar || nextLine[i + 1] == this._escape);
        }

        /// <summary>
        /// precondition: sb.Length() > 0
        /// </summary>
        /// <param name="sb">sb A sequence of characters to examine</param>
        /// <returns>true if every character in the sequence is whitespace</returns>
        protected bool IsAllWhiteSpace(StringBuilder sb)
        {
            bool result = true;

            for (int i = 0; i < sb.Length; i++)
            {
                char c = sb[i];

                if (!char.IsWhiteSpace(c))
                {
                    return false;
                }
            }

            return result;
        }
    }
}
