﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Collections.ObjectModel;

namespace System.Csv
{
    public class CsvReader : IDataReader, IDisposable
    {
        private List<string> _Headers;
        private List<string> _CurrentValues;

        public ReadOnlyCollection<string> Headers { get; set; }
        public ReadOnlyCollection<string> CurrentValues { get; set; }

        private TextReader TextReader { get; set; }

        private bool _isInitalized;
        private bool _isDisposed;
        private bool _isClosed;
        private bool _firstRowContainsHeaders;

        public int NumberOfLinesRead { get; set; }
        public int NumberOfRecordsRead { get; set; }

        private CsvParser _parser;

        public CsvReader(TextReader textReader, CsvParser parser, bool firstRowContainsHeaders)
        {
            if (textReader == null)
                throw new ArgumentNullException("textReader");
            if (parser == null)
                throw new ArgumentNullException("parser");

            _Headers = new List<string>();
            _CurrentValues = new List<string>();
            Headers = new ReadOnlyCollection<string>(_Headers);
            CurrentValues = new ReadOnlyCollection<string>(_CurrentValues);
            _parser = parser;
            TextReader = textReader;
            _isInitalized = true;
            _firstRowContainsHeaders = firstRowContainsHeaders;
        }

        private string Get(int ordinal)
        {
            EnsureInitalized();
            EnsureNotClosed();

            return _CurrentValues[ordinal];
        }
        private bool GetNextRecord()
        {
            string line = TextReader.ReadLine();

            if (line == null)
                return false;

            NumberOfLinesRead++;

            _CurrentValues.Clear();

            string[] values = _parser.ParseLineMulti(line);
            foreach (var item in values)
            {
                _CurrentValues.Add(item.Trim());
            }

            while (_parser.IsPending())
            {
                line = TextReader.ReadLine();

                if (line != null)
                {
                    values = _parser.ParseLineMulti(line);

                    if (line != null)
                        NumberOfLinesRead++;

                    foreach (var item in values)
                    {
                        _CurrentValues.Add(item.Trim());
                    }
                }
            }

            NumberOfRecordsRead++;
            return true;
        }

        #region Open

        public static CsvReader Open(string fileName)
        {
            FileInfo fileInfo = new FileInfo(fileName);

            CsvParser parser = null;

            using (Stream fileStream = fileInfo.OpenRead())
            {
                parser = GetCSVParserBasedOnFile(fileStream);
            }

            Stream fileStream2 = fileInfo.OpenRead();
            StreamReader sr = new StreamReader(fileStream2);
            CsvReader csvReader = new CsvReader(sr, parser, true);
            return csvReader;
        }
        private static CsvParser GetCSVParserBasedOnFile(Stream fileStream)
        {
            CsvParser csvParser = null;

            using (StreamReader reader = new StreamReader(fileStream))
            {
                string line = reader.ReadLine();

                //check first line for tabs or csv
                if (line != null)
                {
                    //if first line contains a tab but does not contain a comma
                    //assume tab delimited
                    if (line.Contains('\t') && !line.Contains(','))
                        csvParser = new CsvParser('\t');
                    else
                        csvParser = new CsvParser();
                }
            }

            return csvParser;
        }

        #endregion

        #region Ensure

        private void EnsureInitalized()
        {
            if (!_isInitalized)
            {
                throw new InvalidOperationException("CSVReader is not initalized");
            }
        }

        private void EnsureNotClosed()
        {
            if (_isClosed)
            {
                throw new InvalidOperationException("CSVReader has already been closed.");
            }

            if (_isDisposed)
            {
                throw new InvalidOperationException("CSVReader has already been disposed.");
            }
        }

        #endregion

        #region IDataReader Members

        /// <summary>
        /// Closes the <see cref="T:System.Data.IDataReader"/> Object.
        /// </summary>
        public void Close()
        {
            TextReader.Close();

            _isClosed = true;
        }

        /// <summary>
        /// Gets a value indicating the depth of nesting for the current row.
        /// </summary>
        /// <value></value>
        /// <returns>
        /// The level of nesting.
        /// </returns>
        public int Depth
        {
            get { return 0; }
        }

        /// <summary>
        /// Returns a <see cref="T:System.Data.DataTable"/> that describes the column metadata of the <see cref="T:System.Data.IDataReader"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Data.DataTable"/> that describes the column metadata.
        /// </returns>
        /// <exception cref="T:System.InvalidOperationException">
        /// The <see cref="T:System.Data.IDataReader"/> is closed.
        /// </exception>
        public DataTable GetSchemaTable()
        {
            DataTable table = new DataTable();

            for (int i = 0; i < Headers.Count; i++)
            {
                string columnName = Headers[i];

                DataColumn column = table.Columns.Add();
                column.DataType = typeof(string);
                column.ColumnName = columnName;
                column.Caption = columnName;
            }

            return table;
        }

        /// <summary>
        /// Gets a value indicating whether the data reader is closed.
        /// </summary>
        /// <value></value>
        /// <returns>true if the data reader is closed; otherwise, false.
        /// </returns>
        public bool IsClosed
        {
            get
            {
                return _isClosed;
            }
        }

        /// <summary>
        /// Advances the data reader to the next result, when reading the results of batch SQL statements.
        /// </summary>
        /// <returns>
        /// true if there are more rows; otherwise, false.
        /// </returns>
        public bool NextResult()
        {
            //there can not be any mutli results here
            return false;
        }

        /// <summary>
        /// Advances the <see cref="T:System.Data.IDataReader"/> to the next record.
        /// </summary>
        /// <returns>
        /// true if there are more rows; otherwise, false.
        /// </returns>
        public bool Read()
        {
            bool result = GetNextRecord();

            if (NumberOfRecordsRead == 1 && _firstRowContainsHeaders) //header
            {
                _Headers.Clear();
                foreach (var header in CurrentValues)
                {
                    _Headers.Add(header.Trim());
                }

                //skip headers and read next line
                NumberOfRecordsRead = 0;
                result = GetNextRecord();
            }

            return result;
        }

        /// <summary>
        /// Gets the number of rows changed, inserted, or deleted by execution of the SQL statement.
        /// </summary>
        /// <value></value>
        /// <returns>
        /// The number of rows changed, inserted, or deleted; 0 if no rows were affected or the statement failed; and -1 for SELECT statements.
        /// </returns>
        public int RecordsAffected
        {
            get
            {
                return -1;
            }
        }

        /// <summary>
        /// Gets the number of columns in the current row.
        /// </summary>
        /// <value></value>
        /// <returns>
        /// When not positioned in a valid recordset, 0; otherwise, the number of columns in the current record. The default is -1.
        /// </returns>
        public int FieldCount
        {
            get
            {
                EnsureInitalized();
                return CurrentValues.Count;
            }
        }

        /// <summary>
        /// Gets the value of the specified column as a Boolean.
        /// </summary>
        /// <param name="i">The zero-based column ordinal.</param>
        /// <returns>The value of the column.</returns>
        /// <exception cref="T:System.IndexOutOfRangeException">
        /// The index passed was outside the range of 0 through <see cref="P:System.Data.IDataRecord.FieldCount"/>.
        /// </exception>
        public bool GetBoolean(int i)
        {
            string value = Get(i);

            if (value != null)
                value = value.Trim();

            return Convert.ToBoolean(value);
        }

        /// <summary>
        /// Gets the 8-bit unsigned integer value of the specified column.
        /// </summary>
        /// <param name="i">The zero-based column ordinal.</param>
        /// <returns>
        /// The 8-bit unsigned integer value of the specified column.
        /// </returns>
        /// <exception cref="T:System.IndexOutOfRangeException">
        /// The index passed was outside the range of 0 through <see cref="P:System.Data.IDataRecord.FieldCount"/>.
        /// </exception>
        public byte GetByte(int i)
        {
            string value = Get(i);

            if (value != null)
                value = value.Trim();

            return Convert.ToByte(value);
        }

        /// <summary>
        /// Reads a stream of bytes from the specified column offset into the buffer as an array, starting at the given buffer offset.
        /// </summary>
        /// <param name="i">The zero-based column ordinal.</param>
        /// <param name="fieldOffset">The index within the field from which to start the read operation.</param>
        /// <param name="buffer">The buffer into which to read the stream of bytes.</param>
        /// <param name="bufferoffset">The index for <paramref name="buffer"/> to start the read operation.</param>
        /// <param name="length">The number of bytes to read.</param>
        /// <returns>The actual number of bytes read.</returns>
        /// <exception cref="T:System.IndexOutOfRangeException">
        /// The index passed was outside the range of 0 through <see cref="P:System.Data.IDataRecord.FieldCount"/>.
        /// </exception>
        public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
        {
            string value = Get(i);

            if (value == null)
                value = string.Empty;

            if (value != null)
                value = value.Trim();

            int bufferPosition = bufferoffset;
            int read = 0;

            for (long k = fieldOffset; k < fieldOffset + length; k++)
            {
                buffer[bufferPosition] = Convert.ToByte(value[(int)k]);
                bufferPosition++;
                read++;
            }

            return read;
        }

        /// <summary>
        /// Gets the character value of the specified column.
        /// </summary>
        /// <param name="i">The zero-based column ordinal.</param>
        /// <returns>
        /// The character value of the specified column.
        /// </returns>
        /// <exception cref="T:System.IndexOutOfRangeException">
        /// The index passed was outside the range of 0 through <see cref="P:System.Data.IDataRecord.FieldCount"/>.
        /// </exception>
        public char GetChar(int i)
        {
            string value = Get(i);

            if (value != null)
                value = value.Trim();

            return Convert.ToChar(value);
        }

        /// <summary>
        /// Gets the chars.
        /// </summary>
        /// <param name="i">The i.</param>
        /// <param name="fieldOffset">The field offset.</param>
        /// <param name="buffer">The buffer.</param>
        /// <param name="bufferoffset">The bufferoffset.</param>
        /// <param name="length">The length.</param>
        /// <returns></returns>
        public long GetChars(int i, long fieldOffset, char[] buffer, int bufferoffset, int length)
        {
            string value = Get(i);

            if (value == null)
                value = string.Empty;

            if (value != null)
                value = value.Trim();

            int bufferPosition = bufferoffset;
            int read = 0;

            for (long k = fieldOffset; k < fieldOffset + length; k++)
            {
                buffer[bufferPosition] = Convert.ToChar(value[(int)k]);
                bufferPosition++;
                read++;
            }

            return read;
        }

        /// <summary>
        /// Returns an <see cref="T:System.Data.IDataReader"/> for the specified column ordinal.
        /// </summary>
        /// <param name="i">The index of the field to find.</param>
        /// <returns>
        /// An <see cref="T:System.Data.IDataReader"/>.
        /// </returns>
        /// <exception cref="T:System.IndexOutOfRangeException">
        /// The index passed was outside the range of 0 through <see cref="P:System.Data.IDataRecord.FieldCount"/>.
        /// </exception>
        public IDataReader GetData(int i)
        {
            EnsureInitalized();
            EnsureNotClosed();

            if (i == 0)
                return this;
            else
                return null;
        }

        /// <summary>
        /// Gets the data type information for the specified field.
        /// </summary>
        /// <param name="i">The index of the field to find.</param>
        /// <returns>
        /// The data type information for the specified field.
        /// </returns>
        /// <exception cref="T:System.IndexOutOfRangeException">
        /// The index passed was outside the range of 0 through <see cref="P:System.Data.IDataRecord.FieldCount"/>.
        /// </exception>
        public string GetDataTypeName(int i)
        {
            EnsureInitalized();
            EnsureNotClosed();
            return typeof(string).FullName;
        }

        /// <summary>
        /// Gets the date and time data value of the specified field.
        /// </summary>
        /// <param name="i">The index of the field to find.</param>
        /// <returns>
        /// The date and time data value of the specified field.
        /// </returns>
        /// <exception cref="T:System.IndexOutOfRangeException">
        /// The index passed was outside the range of 0 through <see cref="P:System.Data.IDataRecord.FieldCount"/>.
        /// </exception>
        public DateTime GetDateTime(int i)
        {
            string value = Get(i);

            if (value != null)
                value = value.Trim();

            return Convert.ToDateTime(value);
        }

        /// <summary>
        /// Gets the fixed-position numeric value of the specified field.
        /// </summary>
        /// <param name="i">The index of the field to find.</param>
        /// <returns>
        /// The fixed-position numeric value of the specified field.
        /// </returns>
        /// <exception cref="T:System.IndexOutOfRangeException">
        /// The index passed was outside the range of 0 through <see cref="P:System.Data.IDataRecord.FieldCount"/>.
        /// </exception>
        public decimal GetDecimal(int i)
        {
            string value = Get(i);

            if (value != null)
                value = value.Trim();

            return Convert.ToDecimal(value);
        }

        /// <summary>
        /// Gets the double-precision floating point number of the specified field.
        /// </summary>
        /// <param name="i">The index of the field to find.</param>
        /// <returns>
        /// The double-precision floating point number of the specified field.
        /// </returns>
        /// <exception cref="T:System.IndexOutOfRangeException">
        /// The index passed was outside the range of 0 through <see cref="P:System.Data.IDataRecord.FieldCount"/>.
        /// </exception>
        public double GetDouble(int i)
        {
            string value = Get(i);

            if (value != null)
                value = value.Trim();

            return Convert.ToDouble(value);
        }

        /// <summary>
        /// Gets the <see cref="T:System.Type"/> information corresponding to the type of <see cref="T:System.Object"/> that would be returned from <see cref="M:System.Data.IDataRecord.GetValue(System.Int32)"/>.
        /// </summary>
        /// <param name="i">The index of the field to find.</param>
        /// <returns>
        /// The <see cref="T:System.Type"/> information corresponding to the type of <see cref="T:System.Object"/> that would be returned from <see cref="M:System.Data.IDataRecord.GetValue(System.Int32)"/>.
        /// </returns>
        /// <exception cref="T:System.IndexOutOfRangeException">
        /// The index passed was outside the range of 0 through <see cref="P:System.Data.IDataRecord.FieldCount"/>.
        /// </exception>
        public Type GetFieldType(int i)
        {
            EnsureInitalized();
            EnsureNotClosed();
            return typeof(string);
        }

        /// <summary>
        /// Gets the single-precision floating point number of the specified field.
        /// </summary>
        /// <param name="i">The index of the field to find.</param>
        /// <returns>
        /// The single-precision floating point number of the specified field.
        /// </returns>
        /// <exception cref="T:System.IndexOutOfRangeException">
        /// The index passed was outside the range of 0 through <see cref="P:System.Data.IDataRecord.FieldCount"/>.
        /// </exception>
        public float GetFloat(int i)
        {
            string value = Get(i);

            if (value != null)
                value = value.Trim();

            return Convert.ToSingle(value);
        }

        /// <summary>
        /// Returns the GUID value of the specified field.
        /// </summary>
        /// <param name="i">The index of the field to find.</param>
        /// <returns>The GUID value of the specified field.</returns>
        /// <exception cref="T:System.IndexOutOfRangeException">
        /// The index passed was outside the range of 0 through <see cref="P:System.Data.IDataRecord.FieldCount"/>.
        /// </exception>
        public Guid GetGuid(int i)
        {
            string value = Get(i);

            if (value != null)
                value = value.Trim();

            return new Guid(value);
        }

        /// <summary>
        /// Gets the 16-bit signed integer value of the specified field.
        /// </summary>
        /// <param name="i">The index of the field to find.</param>
        /// <returns>
        /// The 16-bit signed integer value of the specified field.
        /// </returns>
        /// <exception cref="T:System.IndexOutOfRangeException">
        /// The index passed was outside the range of 0 through <see cref="P:System.Data.IDataRecord.FieldCount"/>.
        /// </exception>
        public short GetInt16(int i)
        {
            string value = Get(i);

            if (value != null)
                value = value.Trim();

            return Convert.ToInt16(value);
        }

        /// <summary>
        /// Gets the 32-bit signed integer value of the specified field.
        /// </summary>
        /// <param name="i">The index of the field to find.</param>
        /// <returns>
        /// The 32-bit signed integer value of the specified field.
        /// </returns>
        /// <exception cref="T:System.IndexOutOfRangeException">
        /// The index passed was outside the range of 0 through <see cref="P:System.Data.IDataRecord.FieldCount"/>.
        /// </exception>
        public int GetInt32(int i)
        {
            string value = Get(i);

            if (value != null)
                value = value.Trim();

            return Convert.ToInt32(value);
        }

        /// <summary>
        /// Gets the 64-bit signed integer value of the specified field.
        /// </summary>
        /// <param name="i">The index of the field to find.</param>
        /// <returns>
        /// The 64-bit signed integer value of the specified field.
        /// </returns>
        /// <exception cref="T:System.IndexOutOfRangeException">
        /// The index passed was outside the range of 0 through <see cref="P:System.Data.IDataRecord.FieldCount"/>.
        /// </exception>
        public long GetInt64(int i)
        {
            string value = Get(i);

            if (value != null)
                value = value.Trim();

            return Convert.ToInt64(value);
        }

        /// <summary>
        /// Gets the name for the field to find.
        /// </summary>
        /// <param name="i">The index of the field to find.</param>
        /// <returns>
        /// The name of the field or the empty string (""), if there is no value to return.
        /// </returns>
        /// <exception cref="T:System.IndexOutOfRangeException">
        /// The index passed was outside the range of 0 through <see cref="P:System.Data.IDataRecord.FieldCount"/>.
        /// </exception>
        public string GetName(int i)
        {
            EnsureInitalized();
            EnsureNotClosed();
            return _Headers[i];
        }

        /// <summary>
        /// Return the index of the named field.
        /// </summary>
        /// <param name="name">The name of the field to find.</param>
        /// <returns>The index of the named field.</returns>
        public int GetOrdinal(string name)
        {
            for (int i = 0; i < Headers.Count; i++)
            {
                string header = _Headers[i];
                if (name.Equals(header, StringComparison.CurrentCultureIgnoreCase))
                    return i;
            }

            return -1;
        }

        /// <summary>
        /// Gets the string value of the specified field.
        /// </summary>
        /// <param name="i">The index of the field to find.</param>
        /// <returns>The string value of the specified field.</returns>
        /// <exception cref="T:System.IndexOutOfRangeException">
        /// The index passed was outside the range of 0 through <see cref="P:System.Data.IDataRecord.FieldCount"/>.
        /// </exception>
        public string GetString(int i)
        {
            string value = Get(i);
            return value;
        }

        /// <summary>
        /// Return the value of the specified field.
        /// </summary>
        /// <param name="i">The index of the field to find.</param>
        /// <returns>
        /// The <see cref="T:System.Object"/> which will contain the field value upon return.
        /// </returns>
        /// <exception cref="T:System.IndexOutOfRangeException">
        /// The index passed was outside the range of 0 through <see cref="P:System.Data.IDataRecord.FieldCount"/>.
        /// </exception>
        public object GetValue(int i)
        {
            return Get(i);
        }

        /// <summary>
        /// Gets all the attribute fields in the collection for the current record.
        /// </summary>
        /// <param name="values">An array of <see cref="T:System.Object"/> to copy the attribute fields into.</param>
        /// <returns>
        /// The number of instances of <see cref="T:System.Object"/> in the array.
        /// </returns>
        public int GetValues(object[] values)
        {
            EnsureInitalized();
            EnsureNotClosed();

            for (int i = 0; i < CurrentValues.Count; i++)
            {
                values[i] = CurrentValues[i];
            }

            return _CurrentValues.Count;
        }

        /// <summary>
        /// Return whether the specified field is set to null.
        /// </summary>
        /// <param name="i">The index of the field to find.</param>
        /// <returns>
        /// true if the specified field is set to null; otherwise, false.
        /// </returns>
        /// <exception cref="T:System.IndexOutOfRangeException">
        /// The index passed was outside the range of 0 through <see cref="P:System.Data.IDataRecord.FieldCount"/>.
        /// </exception>
        public bool IsDBNull(int i)
        {
            return string.IsNullOrEmpty(Get(i));
        }

        /// <summary>
        /// Gets the <see cref="System.Object"/> with the specified name.
        /// </summary>
        /// <value></value>
        public object this[string name]
        {
            get
            {
                int ordinal = GetOrdinal(name);
                return this[ordinal];
            }
        }

        /// <summary>
        /// Gets the <see cref="System.Object"/> with the specified i.
        /// </summary>
        /// <value></value>
        public object this[int i]
        {
            get
            {
                return Get(i);
            }
        }

        #endregion

        public void Dispose()
        {
            Close();
            _isDisposed = true;
        }
    }
}
