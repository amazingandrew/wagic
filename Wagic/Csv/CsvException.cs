﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Csv
{
    [Serializable()]
    public class CsvException : Exception
    {
        /// <summary>
        /// Initializes a new <see cref="CsvException"/> class.
        /// </summary>
        public CsvException()
            : base()
        {

        }

        /// <summary>
        /// Initializes a new <see cref="CsvException"/> class with a specified error message.
        /// </summary>
        /// <param name="message">A message that describes the error.</param>
        public CsvException(string message)
            : base(message)
        {

        }

        /// <summary>
        /// Initializes a new <see cref="CsvException"/> class with a specified error.
        /// message and a reference to the inner exception that is the cause of this exception.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.
        /// </param>
        /// <param name="exception">The exception that is the cause of the current exception. 
        /// If the innerException parameter is not a null reference, the current exception 
        /// is raised in a catch block that handles the inner exception.
        /// </param>
        public CsvException(string message, Exception exception) :
            base(message, exception)
        {

        }
    }
}
