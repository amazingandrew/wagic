﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Wagic
{
    public class WagicPaths
    {
        public const string RESOURCE_FOLDER = "Res";
        public const string USER_FOLDER = "User";
        public const string PROFILES = "User\\profiles";
        public const string PROFILES_DEFAULT = "User\\player";
        public const string USER_SETS = "User\\sets";
        
        public const string PROFILES_COLLECTION_FILE = "collection.dat";
        public const string PROFILES_DATA_FILE = "data.dat";
        public const string PROFILES_OPTIONS_FILE = "options.txt";

        public static bool IsValidWagicRoot(string wagicRoot)
        {
            //we expect to see a "RES" and a "USER" Folder
            //we also expect to see a default profile folder

            if (Directory.Exists(GetResourceFolder(wagicRoot)) &&
                Directory.Exists(GetUserFolder(wagicRoot)) &&
                Directory.Exists(GetDefaultProfile(wagicRoot)))
                return true;
            else
                return false;
        }

        public static string GetDefaultProfile(string wagicRoot)
        {
            return Path.Combine(wagicRoot, PROFILES_DEFAULT);
        }
        public static string GetProfilesFolder(string wagicRoot)
        {
            return Path.Combine(wagicRoot, PROFILES);
        }
        
        public static string GetResourceFolder(string wagicRoot)
        {
            return Path.Combine(wagicRoot, RESOURCE_FOLDER);
        }
        public static string GetUserFolder(string wagicRoot)
        {
            return Path.Combine(wagicRoot, USER_FOLDER);
        }
        public static string GetUserSetFolder(string wagicRoot, string setName)
        {
            return Path.Combine(Path.Combine(wagicRoot, USER_SETS), setName);
        }

        public static IList<string> GetProfileDecks(string profileRoot)
        {
            int i = 1;

            string deckName = string.Format("deck{0}.txt", i);
            string fullDeckName = Path.Combine(profileRoot, deckName);
            List<string> results = new List<string>();

            while (File.Exists(fullDeckName))
            {
                results.Add(fullDeckName);

                i++;
                deckName = string.Format("deck{0}.txt", i);
                fullDeckName = Path.Combine(profileRoot, deckName);
            }

            return results;
        }
        public static string GetNextAvailableDeckName(string profileRoot)
        {
            int i = 1;

            string deckName = string.Format("deck{0}.txt", i);
            string fullDeckName = Path.Combine(profileRoot, deckName);

            while (File.Exists(fullDeckName))
            {
                i++;
                deckName = string.Format("deck{0}.txt", i);
                fullDeckName = Path.Combine(profileRoot, deckName);
            }

            return fullDeckName;
        }
        public static string GetProfileCollectionFile(string profileRoot)
        {
            return Path.Combine(profileRoot, PROFILES_COLLECTION_FILE);
        }
        public static string GetProfileDataFile(string profileRoot)
        {
            return Path.Combine(profileRoot, PROFILES_DATA_FILE);
        }
        public static string GetProfileOptionsFile(string profileRoot)
        {
            return Path.Combine(profileRoot, PROFILES_OPTIONS_FILE);
        }


    }
}
