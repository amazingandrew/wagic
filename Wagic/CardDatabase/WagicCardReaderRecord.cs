﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;

namespace Wagic.CardDatabase
{
    public class WagicCardReaderRecord
    {
        public string RecordType { get; set; }
        public NameValueCollection Properties { get; private set; }

        public WagicCardReaderRecord()
        {
            Properties = new NameValueCollection();
        }

        public string Get(string key)
        {
            key = key.ToLower();

            if (Properties.AllKeys.Contains(key))
                return Properties[key];
            else
                return null;            
        }
        public string[] GetAll(string key)
        {
            key = key.ToLower();

            if (Properties.AllKeys.Contains(key))
                return Properties.GetValues(key);
            else
                return null;            
        }
    }
}
