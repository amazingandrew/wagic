﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Wagic.CardDatabase
{
    public class CardSetReader
    {
        public CardSet ReadStream(string setName, Stream s)
        {
            CardSet set = new CardSet();
            set.Name = setName;

            using (WagicCardReader r = new WagicCardReader(s))
            while(r.ReadNextRecord())
            {
                var rec = r.CurrentRecord;

                rec.RecordType = rec.RecordType ?? "(null)";

                switch (rec.RecordType.ToLower().Trim())
                {
                    case "meta":
                        set.Author = rec.Get("author");
                        set.Block = rec.Get("block");
                        set.Description = rec.Get("name");
                        set.Year = rec.Get("year");
                        break;
                    case "card":
                        CardInfo card = new CardInfo();

                        if (rec.Get("id") != null && rec.Get("id").Trim() != "")
                            card.ID = long.Parse(rec.Get("id"));
                        card.PrimitiveName = rec.Get("primitive");
                        card.Rarity = rec.Get("rarity");
                            
                        card.Name = rec.Get("name");
                        card.Type = rec.Get("type");
                        card.Text = rec.Get("text");
                        card.Mana = rec.Get("mana");
                        card.Power = rec.Get("power");
                        card.Toughness = rec.Get("toughness");
                        
                        AddList(rec, card.Auto, "auto");
                        AddList(rec, card.AutoGraveyard, "autograveyard");
                        AddList(rec, card.Color, "color");
                        AddList(rec, card.Subtypes, "subtype");
                        AddCSV(rec, card.Abilities, "abilities");

                        card.Set = set.Name;
                        set.Cards.Add(card);
                        break;
                    default:
                        break;
                }
            }

            return set;
        }

        private static void AddList(WagicCardReaderRecord rec, List<string> list, string fieldName)
        {
            var all = rec.GetAll(fieldName);

            if (all != null)
                foreach (var item in all)
                    list.Add(item);
        }
        private static void AddCSV(WagicCardReaderRecord rec, List<string> list, string fieldName)
        {
            string s = rec.Properties[fieldName];

            if (s != null)
            {
                var values = s.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var item in values)
                {
                    list.Add(item);
                }
            }
        }
    }
}
