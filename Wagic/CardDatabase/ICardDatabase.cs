﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wagic.CardDatabase
{
    public interface ICardDatabase
    {
        CardInfo FindCardByID(long id);
        List<CardSet> GetAllCards();
        
        CardInfo FindCard(string setName, string cardName);
        CardInfo FindCard(string cardName);
    }
}
