﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wagic.CardDatabase
{
    //Analysis from sets
    //id: 43257 items
    //rarity: 43237 items
    //primitive: 42157 items
    //name: 1100 items
    //type: 1100 items
    //text: 1026 items
    //mana: 953 items
    //auto: 902 items
    //subtype: 766 items
    //power: 571 items
    //toughness: 570 items
    //abilities: 340 items
    //target: 168 items
    //color: 128 items
    //alias: 21 items
    //autograveyard: 18 items
    //kicker: 7 items
    //autohand: 3 items
    //colour: 3 items
    //autoexile: 1 items
    //autolibrary: 1 items
    //subtye: 1 items
    //buyback: 1 items
    //abilitie: 1 items

    //Maximum values in a single record
    //auto          8
    //subtype       2
    //autograveyard 2
    //color         2
    //primitive     1
    //id            1
    //rarity        1
    //name          1
    //abilities     1
    //text          1
    //mana          1
    //kicker        1
    //type          1
    //power         1
    //toughness     1
    //autoexile     1
    //autohand      1
    //autolibrary   1
    //target        1
    //alias         1
    //subtye        1
    //buyback       1
    //colour        1
    //abilitie      1

    public class CardInfo
    {
        public string Set { get; set; }

        public long ID { get; set; }
        public string Rarity { get; set; }
        public string PrimitiveName { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }
        public string Text { get; set; }
        public string Mana { get; set; }
        public string Power { get; set; }
        public string Toughness { get; set; }

        public List<string> Auto { get; private set; }
        public List<string> AutoGraveyard { get; private set; }
        public List<string> Abilities { get; private set; }
        public List<string> Subtypes { get; private set; }
        public List<string> Color { get; private set; }
        
        public CardInfo()
        {
            Auto = new List<string>();
            AutoGraveyard = new List<string>();
            Abilities = new List<string>();
            Subtypes = new List<string>();
            Color = new List<string>();
        }

        public override string ToString()
        {
            string pt = "";

            if (Power != null && Power.Trim() != "")
                pt = string.Format(" {0}/{1}", Power, Toughness);

            return string.Format("Card: ({0}) {1} {2} {3}{4}",
                ID, Rarity, Name, Type, pt);
        }
    }
}
