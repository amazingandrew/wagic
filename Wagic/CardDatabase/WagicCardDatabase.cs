﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Wagic.CardDatabase
{
    public class WagicCardDatabase : ICardDatabase
    {
        public const string MASTER_DATABASE_FILENAME = "mtg.txt";

        public DirectoryInfo WagicRoot { get; private set; }

        public WagicCardDatabase(string directoryPath)
        {
            if (directoryPath == null)
                throw new ArgumentNullException("directoryPath");
            if (directoryPath.Trim() == string.Empty)
                throw new ArgumentException("directoryPath");

            WagicRoot = new DirectoryInfo(directoryPath);
        }

        
        private List<CardSet> InternalReadSets()
        {
            Wagic.CardDatabase.CardSetReader cardSetReader = new CardDatabase.CardSetReader();

            //read any zip files in root of RES
            DirectoryInfo RootRes = GetSubFolder(WagicRoot, "Res");
            DirectoryInfo UserRes = GetSubFolder(WagicRoot, "User");

            var Root_ZipFiles = RootRes.GetFiles("*.zip", SearchOption.TopDirectoryOnly);

            List<CardSet> AllSets = new List<CardSet>();

            //Read Root Zip Files
            foreach (var zipFile in Root_ZipFiles)
            {
                Ionic.Zip.ZipFile z = new Ionic.Zip.ZipFile(zipFile.FullName);

                //find all the card data files
                var entries = z.SelectEntries("_cards.dat", null);

                foreach (var item in entries)
                {
                    string setName = Path.GetFileNameWithoutExtension(Path.GetDirectoryName(item.FileName));

                    //read file into memory and reset position
                    MemoryStream memory = new MemoryStream();
                    item.Extract(memory);
                    memory.Position = 0;

                    Console.WriteLine("Reading {0} ({1})", zipFile.FullName, item.FileName);
                    var cardSet = cardSetReader.ReadStream(setName, memory);
                    AllSets.Add(cardSet);
                }
            }

            //Read Program Res
            var Prog_Set = GetSubFolder(RootRes, "sets");
            if (Prog_Set.Exists)
            {
                var files = Prog_Set.GetFiles("_cards.dat", SearchOption.AllDirectories);
                foreach (var item in files)
                {
                    Console.WriteLine("Reading {0}", item.FullName);
                    string setName = Path.GetFileNameWithoutExtension(Path.GetDirectoryName(item.FullName));
                    var cardSet = cardSetReader.ReadStream(setName, item.OpenRead());
                    AllSets.Add(cardSet);
                }
            }
            

            //Read User Res
            var User_Set = GetSubFolder(UserRes, "sets");
            if (User_Set.Exists)
            {
                var files = User_Set.GetFiles("_cards.dat", SearchOption.AllDirectories);
                foreach (var item in files)
                {
                    Console.WriteLine("Reading {0}", item.FullName);
                    string setName = Path.GetFileNameWithoutExtension(Path.GetDirectoryName(item.FullName));
                    var cardSet = cardSetReader.ReadStream(setName, item.OpenRead());
                    AllSets.Add(cardSet);
                }
            }
            

            //Merge everything into a new master list
            Dictionary<string, CardSet> MasterList = new Dictionary<string, CardSet>();

            foreach (var set in AllSets)
            {
                if (!MasterList.ContainsKey(set.Name))
                {
                    MasterList.Add(set.Name, set);
                }
                else
                {
                    //only add missing cards
                    CardSet masterSet = MasterList[set.Name];

                    foreach (var card in set.Cards)
                    {
                        if (masterSet.Cards.Where(x => x.ID == card.ID).Count() == 0)
                        {
                            masterSet.Cards.Add(card);
                        }
                    }
                }
            }



            //READ PRIMITIVE DATA
            var primitives = ReadPrimitivesFile();
            var missingPrimitives = new List<CardInfo>();
            foreach (var set in MasterList.Values)
                foreach (var card in set.Cards)
                {
                    if (card.PrimitiveName != null)
                    {
                        if (primitives.ContainsKey(card.PrimitiveName))
                        {
                            CardInfo p = primitives[card.PrimitiveName];
                            MergeWithPrimitive(card, p);
                        }
                        else
                        {
                            missingPrimitives.Add(card);
                        }                        
                    }
                }

            //remove missing primitve cards
            foreach (var item in missingPrimitives)
            {
                MasterList[item.Set].Cards.Remove(item);
            }

            return MasterList.Values.ToList();
        }

        #region Helper Methods

        private static DirectoryInfo GetSubFolder(DirectoryInfo dir, string subFolderName)
        {
            return new DirectoryInfo(Path.Combine(dir.FullName, subFolderName));
        }
        private Dictionary<string, CardInfo> ReadPrimitivesFile()
        {
            CardSetReader reader = new CardSetReader();

            Dictionary<string, CardInfo> dic = new Dictionary<string, CardInfo>();

            var RootPrimities = GetSubFolder(WagicRoot, "Res/sets/primitives");
            var UserPrimitives = GetSubFolder(WagicRoot, "User/sets/primitives");
            var Root_ZipFiles = GetSubFolder(WagicRoot, "Res").GetFiles("*.zip", SearchOption.TopDirectoryOnly);

            //look for primities in zip files and root and user 
            if (RootPrimities.Exists)
            {
                var file = RootPrimities.GetFiles(MASTER_DATABASE_FILENAME).FirstOrDefault();
                if (file != null && file.Exists)
                {
                    var set = reader.ReadStream("PRIMITIVES", file.OpenRead());
                    foreach (var item in set.Cards)
                    {
                        //check to see if primitive already exists.
                        if (dic.ContainsKey(item.Name))
                        {
                            var other = dic[item.Name];
                            //TODO: think about combinging these??
                        }
                        else
                        {
                            dic.Add(item.Name, item);
                        }
                    }
                }
            }

            if (UserPrimitives.Exists)
            {
                var file = UserPrimitives.GetFiles(MASTER_DATABASE_FILENAME).FirstOrDefault();
                if (file != null && file.Exists)
                {
                    var set = reader.ReadStream("PRIMITIVES", file.OpenRead());
                    foreach (var item in set.Cards)
                    {
                        //check to see if primitive already exists.
                        if (dic.ContainsKey(item.Name))
                        {
                            var other = dic[item.Name];
                            //TODO: think about combinging these??
                        }
                        else
                        {
                            dic.Add(item.Name, item);
                        }
                    }
                }
            }


            //Read Root Zip Files
            foreach (var zipFile in Root_ZipFiles)
            {
                Ionic.Zip.ZipFile z = new Ionic.Zip.ZipFile(zipFile.FullName);

                //find all the card data files
                var entries = z.SelectEntries(MASTER_DATABASE_FILENAME, "sets/primitives");

                foreach (var zipEntry in entries)
                {
                    //read file into memory and reset position
                    MemoryStream memory = new MemoryStream();
                    zipEntry.Extract(memory);
                    memory.Position = 0;

                    var set = reader.ReadStream("PRIMITIVES", memory);
                    foreach (var item in set.Cards)
                    {
                        //check to see if primitive already exists.
                        if (dic.ContainsKey(item.Name))
                        {
                            var other = dic[item.Name];
                            //TODO: think about combinging these??
                        }
                        else
                        {
                            dic.Add(item.Name, item);
                        }
                    }
                }
            }


            return dic;
        }
        private static void MergeWithPrimitive(CardInfo card, CardInfo primitive)
        {
            card.Abilities.AddRange(primitive.Abilities);
            card.Auto.AddRange(primitive.Auto);
            card.AutoGraveyard.AddRange(primitive.AutoGraveyard);
            card.Color.AddRange(primitive.Color);
            card.Mana = primitive.Mana;
            card.Name = primitive.Name;
            card.Power = primitive.Power;
            card.Subtypes.AddRange(primitive.Subtypes);
            card.Text = primitive.Text;
            card.Toughness = primitive.Toughness;
            card.Type = primitive.Type;
        }

        #endregion

        private IList<CardSet> CACHE { get; set; }
        private Dictionary<long, CardInfo> CardIdLookup { get; set; }
        private void SetInternalData()
        {
            CACHE = InternalReadSets();

            CardIdLookup = new Dictionary<long, CardInfo>();
            foreach (var item in CACHE.SelectMany(x => x.Cards))
            {
                if (!CardIdLookup.ContainsKey(item.ID))
                    CardIdLookup.Add(item.ID, item);
            }
        }                

        public CardInfo FindCardByID(long id)
        {
            if (CACHE == null)
                SetInternalData();

            if (CardIdLookup.ContainsKey(id))
                return CardIdLookup[id];
            else
                return null;
        }
        public List<CardSet> GetAllCards()
        {
            if (CACHE == null)
                SetInternalData();

            return new List<CardSet>(CACHE);
        }


        public CardInfo FindCard(string setName, string cardName)
        {
            if (CACHE == null)
                SetInternalData();

            return CardIdLookup.Select(x => x.Value).Where(x => x.Set == setName && x.Name == cardName).FirstOrDefault();
        }
        public CardInfo FindCard(string cardName)
        {
            if (CACHE == null)
                SetInternalData();

            return CardIdLookup.Select(x => x.Value).Where(x => x.Name == cardName).FirstOrDefault();
        }
    }
}
