﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wagic.CardDatabase
{
    public class CardSet
    {
        public string Author { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Year { get; set; }
        public string Block { get; set; }

        public IList<CardInfo> Cards { get; private set; }

        public CardSet()
        {
            Cards = new List<CardInfo>();
        }

        public override string ToString()
        {
            return string.Format("CardSet: {0} ({1}) {1} cards",
                Name, Year, Cards.Count);
        }
    }
}
