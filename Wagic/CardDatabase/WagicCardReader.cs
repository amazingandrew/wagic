﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;

namespace Wagic.CardDatabase
{
    public class WagicCardReader : IDisposable
    {
        public const string Regex_Record = @"^\[(\w+)\]$";
        public StreamReader InternalStreamReader { get; private set; }

        public WagicCardReaderRecord CurrentRecord { get; private set; }

        public WagicCardReader(Stream s)
        {
            InternalStreamReader = new StreamReader(s);
        }

        public bool ReadNextRecord()
        {
            CurrentRecord = null;

            string recordType = null;
            
            //read until new record starts
            string line = InternalStreamReader.ReadLine();
            while (recordType == null)
            {
                if (line == null || InternalStreamReader.EndOfStream)
                {
                    return false;
                }

                if (line.Trim().StartsWith("#"))
                {
                    line = InternalStreamReader.ReadLine();
                    continue;
                }

                if (Regex.IsMatch(line, Regex_Record))
                {
                    recordType = line.Remove(0, 1).Remove(line.Length - 2);
                    break;
                }

                line = InternalStreamReader.ReadLine();
            }

            
            //read properties until end record
            string endRecord = "[/" + recordType + "]";

            //read next record
            line = InternalStreamReader.ReadLine();

            List<KeyValuePair<string, string>> propList = new List<KeyValuePair<string, string>>();

            while (!line.Trim().StartsWith(endRecord)) //read until EOR
            {
                if (InternalStreamReader.EndOfStream)
                {
                    //TODO: throw error invalid format
                }

                //if we match a new start, just finish 
                if (Regex.IsMatch(line, Regex_Record))
                {
                    break;
                }

                //skip lines that start with # (comment)
                if (!line.StartsWith("#"))
                {
                    int equalsIndex = line.IndexOf("=");

                    if (equalsIndex == -1) //if we don't have an equals add to last property
                    {
                        if (propList.Count > 0)
                        {
                            var last = propList.Last();
                            propList.Remove(last);
                            propList.Add(new KeyValuePair<string, string>(last.Key,
                                last.Value + Environment.NewLine + line));
                        }
                    }
                    else
                    {
                        string key = line.Substring(0, equalsIndex);
                        string value = line.Remove(0, equalsIndex + 1);

                        //lowercase all keys
                        key = key.ToLower();
                        propList.Add(new KeyValuePair<string,string>(key, value));
                    }
                }

                line = InternalStreamReader.ReadLine();
            }

            //return record
            WagicCardReaderRecord record = new WagicCardReaderRecord();
            record.RecordType = recordType;
            foreach (var item in propList)
            {
                record.Properties.Add(item.Key, item.Value);
            }

            CurrentRecord = record;
            return true;
        }

        public void Dispose()
        {
            InternalStreamReader.Dispose();
        }
    }
}
