﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Wagic.CardDatabase;

namespace Wagic.Translation
{
    public class SetNameTranslator : ISetNameTranslator
    {
        public ICardDatabase DB { get; private set; }
        public IList<string> KnownSetNames { get; private set; }
        public IList<SetNameTranslatorData> SetData { get; private set; }
                
        public SetNameTranslator(ICardDatabase cardDatabase, string setDataFileName)
        {
            if (cardDatabase == null)
                throw new ArgumentNullException("cardDatabase");

            DB = cardDatabase;
            KnownSetNames = DB.GetAllCards().SelectMany(x => x.Cards).Select(x => x.Set).Distinct().ToList();
            SetData = SetNameTranslatorData.ParseSetFile(setDataFileName);
        }

        public string FindProperSetName(string setName)
        {
            if (KnownSetNames.Contains(setName))
                return setName;

            foreach (var setInfo in SetData)
            {
                if (setInfo.Aliases.Contains(setName))
                {
                    foreach (var alias in setInfo.Aliases)
                    {
                        if (KnownSetNames.Contains(alias))
                            return alias;
                    }
                }
            }

            //unknown set name, just return given
            return setName;
        }
    }
}
