﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wagic.Translation
{
    public interface ISetNameTranslator
    {
        string FindProperSetName(string setName);
    }
}
