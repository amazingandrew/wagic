﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Wagic.Translation
{
    public class SetNameTranslatorData
    {
        public string Description { get; set; }
        public IList<string> Aliases { get; private set; }

        public SetNameTranslatorData()
        {
            Aliases = new List<string>();
        }

        public static List<SetNameTranslatorData> ParseSetFile(string fileName)
        {
            List<SetNameTranslatorData> list = new List<SetNameTranslatorData>();

            using (Stream s = Util.OpenRead(fileName))
            using (StreamReader sr = new StreamReader(s))
            {
                string line = sr.ReadLine();

                while (line != null)
                {
                    if (line.Trim() != "" && !line.StartsWith("//") && !line.StartsWith("#"))
                    {
                        string[] split = line.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

                        SetNameTranslatorData setInfo = new SetNameTranslatorData();
                        foreach (var item in split)
                        {
                            setInfo.Aliases.Add(item);
                        }
                        setInfo.Description = setInfo.Aliases.Last();

                        list.Add(setInfo);
                    }

                    line = sr.ReadLine();
                }
            }

            return list;
        }
    }
}
