﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Wagic.CardDatabase;

namespace Wagic.Decks
{
    public class Deck
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public IList<DeckCard> Cards { get; private set; }

        public Deck()
        {
            Cards = new List<DeckCard>();
        }

        public void AddToDeck(CardInfo card)
        {
            //see if we already have card in deck
            var existing = this.Cards.Where(x => x.ID == card.ID).FirstOrDefault();

            if (existing != null)
            {
                existing.Quantity++;
            }
            else
            {
                DeckCard deckCard = new DeckCard();
                deckCard.ID = card.ID;
                deckCard.Name = card.Name;
                deckCard.Set = card.Set;
                deckCard.Quantity = 1;

                this.Cards.Add(deckCard);
            }
        }
        public void AddToDeck(DeckCard card)
        {
            //see if we already have card in deck
            var existing = this.Cards.Where(x => x.ID == card.ID).FirstOrDefault();

            if (existing != null)
            {
                existing.Quantity += card.Quantity;
            }
            else
            {
                this.Cards.Add(card);
            }
        }

        public override string ToString()
        {
            return string.Format("Deck: {0}, {1} unique, {2} total",
                Name, Cards.Count, Cards.Sum(x => x.Quantity));
        }
    }
}
