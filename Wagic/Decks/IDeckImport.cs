﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Wagic.Decks
{
    public interface IDeckImport
    {
        Deck Import(Stream stream);
    }
}
