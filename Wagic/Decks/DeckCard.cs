﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wagic.Decks
{
    public class DeckCard
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public string Set { get; set; }
        public int Quantity { get; set; }
        public bool IsSideboard { get; set; }

        public override string ToString()
        {
            return string.Format("({0}) {1} {2} {3}",
                ID, Set, Name, Quantity);
        }
    }

}
