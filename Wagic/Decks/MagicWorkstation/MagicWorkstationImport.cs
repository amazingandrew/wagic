﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Wagic.CardDatabase;
using Wagic.Translation;
using System.Text.RegularExpressions;

namespace Wagic.Decks.MagicWorkstation
{
    public class MagicWorkstationImport : IDeckImport
    {
        public ICardDatabase CardDatabase { get; private set; }
        public ISetNameTranslator SetNameTranslator { get; private set; }
        public bool ReadSideboards { get; private set; }
                
        public MagicWorkstationImport(ICardDatabase cardDatabase, ISetNameTranslator setNameTranslator, bool readSideboards)
        {
            if (cardDatabase == null)
                throw new ArgumentNullException("cardDatabase");
            if (setNameTranslator == null)
                throw new ArgumentNullException("setNameTranslator");

            CardDatabase = cardDatabase;
            SetNameTranslator = setNameTranslator;
            ReadSideboards = readSideboards;
        }

        public Deck Import(System.IO.Stream stream)
        {
            Deck deck = new Deck();
            StreamReader sr = new StreamReader(stream);

            string line = sr.ReadLine();

            while (line != null)
            {
                if (!line.StartsWith("//") && line.Trim() != "")
                {
                    string recRegex = @"(?<sb>SB:)?\s*(?<quantity>\d+)\s*\[(?<set>\w+)\]\s*(?<name>.+)";
                    var m = Regex.Match(line, recRegex);

                    if (m.Success)
                    {
                        string name = m.Groups["name"].Value;
                        string set = m.Groups["set"].Value;
                        string quan = m.Groups["quantity"].Value;

                        DeckCard c = new DeckCard();
                        c.Name = NormalizeName(name.Trim());
                        c.Quantity = int.Parse(quan);
                        c.Set = set;

                        if (m.Groups["sb"].Success)
                            c.IsSideboard = true;


                        if (c.IsSideboard)
                        {
                            if (this.ReadSideboards)
                                deck.Cards.Add(c);
                        }
                        else
                        {
                            deck.Cards.Add(c);
                        }
                    }
                    else
                    {
                        //unknown record format
                    }

                }

                line = sr.ReadLine();
            }

            //Translate Deck
            TranslateDeck(deck);

            return deck;
        }

        private void TranslateDeck(Deck deck)
        {
            //first try to find the proper set name
            foreach (var card in deck.Cards)
            {
                card.Set = SetNameTranslator.FindProperSetName(card.Set);
                card.ID = FindIdForCard(card);
            }
        }

        private string NormalizeName(string p)
        {
            return Regex.Replace(p, @"\(\d+\)", "").Trim();
        }

        public long FindIdForCard(DeckCard card)
        {
            var found = CardDatabase.FindCard(card.Set, card.Name);

            //could not find card, look for it in another set
            if (found == null)
                found = CardDatabase.FindCard(card.Name);

            if (found != null)
            {
                //find card in set
                return found.ID;
            }
            else
            {
                //unknown set
                return -1;
            }
        }
    }
}
