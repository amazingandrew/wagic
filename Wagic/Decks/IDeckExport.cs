﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Wagic.Decks
{
    public interface IDeckExport
    {
        Deck Export(Stream stream, Deck deck);
    }
}
