﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Wagic.Decks.Formats
{
    public interface IDeckFormat
    {
        Deck ReadDeck(Stream stream);
        void WriteDeck(Stream stream, Deck deck);
    }
}
