﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using Wagic.CardDatabase;
using Wagic.Translation;

namespace Wagic.Decks.Formats
{
    public class WagicDeckFormat : IDeckFormat
    {
        public ICardDatabase CardDatabase { get; private set; }
        
        public WagicDeckFormat(ICardDatabase cardDatabase)
        {
            CardDatabase = cardDatabase;
        }

        public Deck ReadDeck(System.IO.Stream stream)
        {
            Deck deck = new Deck();
            StreamReader sr = new StreamReader(stream);

            string line = sr.ReadLine();
            List<string> descLines = new List<string>();

            while (line != null)
            {
                if (line.StartsWith("#NAME:"))
                    deck.Name = line.Substring(line.IndexOf(":") + 1);
                if (line.StartsWith("#DESC"))
                    descLines.Add(line.Substring(line.IndexOf(":") + 1).Trim());

                if (line.Trim() != "" && !line.StartsWith("#"))
                {
                    //if this is a all integer get data from ID
                    if (Regex.IsMatch(line, @"^\d+$"))
                    {
                        long id = long.Parse(line.Trim());

                        var card = CardDatabase.FindCardByID(id);
                        if (card != null)
                        {
                            deck.AddToDeck(card);
                        }
                        else
                        {
                            //TODO: Unknown card ID
                            Console.WriteLine("Unknown card id {0}", id);
                        }
                    }
                    else //NAN figure it out
                    {
                        string cardRegex = @"(?<name>.+)\((?<set>\w+)\)\s*\*\s*(?<quantity>\d+)";
                        var match = Regex.Match(line, cardRegex);

                        if (match.Success)
                        {
                            DeckCard card = new DeckCard();
                            card.Name = match.Groups["name"].Value.Trim();
                            card.Set = match.Groups["set"].Value.Trim();
                            card.Quantity = int.Parse(match.Groups["quantity"].Value);

                            FindIdForCard(card);

                            //check id
                            if (card.ID == -1)
                            {
                                //invalid id skip
                                //TODO: hanlde coulnd't find ID
                            }
                            else
                            {
                                deck.AddToDeck(card);
                            }
                        }
                        else
                        {
                            //TODO: unknown line format
                        }
                    }
                }

                line = sr.ReadLine();
            }

            if (descLines.Count > 0)
                deck.Description = string.Join(Environment.NewLine, descLines);

            return deck;
        }

        private void FindIdForCard(DeckCard card)
        {
            var found = CardDatabase.FindCard(card.Set, card.Name);
            
            //could not find card, look for it in another set
            if (found == null)
                found = CardDatabase.FindCard(card.Name);

            if (found != null)
            {
                //find card in set
                card.ID = found.ID;
            }
            else
            {
                //unknown set
                card.ID = -1;
            }
        }

        public void WriteDeck(System.IO.Stream stream, Deck deck)
        {
            var writer = Util.GetWriter(stream);
            writer.WriteLine(string.Format("#NAME:{0}", deck.Name));

            if (deck.Description != null && deck.Description.Trim() != "")
            {
                string[] lines = deck.Description.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
                foreach (var desc in lines)
                {
                    writer.WriteLine(string.Format("#DESC:{0}", desc));
                }
            }

            //create card groups            
            List<DeckCard> unkownCards = new List<DeckCard>();
            List<DeckCard> creatureCards = new List<DeckCard>();
            List<DeckCard> landCards = new List<DeckCard>();
            List<DeckCard> otherCards = new List<DeckCard>();

            //Group the cards together
            foreach (var card in deck.Cards)
            {
                //try to find IDs for cards
                if (card.ID <= 0)
                    FindIdForCard(card);
                
                if (card.ID <= 0)
                {
                    //unknown card
                    unkownCards.Add(card);
                }
                else
                {
                    CardInfo info = CardDatabase.FindCardByID(card.ID);

                    if (info.Type.ToLower().Contains("land"))
                        landCards.Add(card);
                    else if (info.Type.ToLower().Contains("creature"))
                        creatureCards.Add(card);
                    else 
                        otherCards.Add(card);                        
                }
            }


            bool WriteIDMode = false;
            WriteCardSection(WriteIDMode, writer, creatureCards, "CREATURES");
            WriteCardSection(WriteIDMode, writer, otherCards, "SPELLS");
            WriteCardSection(WriteIDMode, writer, landCards, "LANDS");

            //write unkown
            if (unkownCards.Count > 0)
            {
                writer.WriteLine("# Unknown Cards ({0} cards)", unkownCards.Sum(x => x.Quantity));
                foreach (var card in unkownCards)
                {
                    writer.WriteLine("#UNKNOWN: " + string.Format("{0,-30}{1,-5} *{2}", card.Name, "(" + card.Set + ")", card.Quantity));
                }
                writer.WriteLine();
            }

            writer.Flush();
        }

        private void WriteCardSection(bool WriteIDMode, StreamWriter writer, List<DeckCard> creatureCards, string header)
        {
            writer.WriteLine("# {0} ({1} cards)", header, creatureCards.Sum(x => x.Quantity));
            foreach (var card in creatureCards.OrderBy(x => x.Name))
            {
                WriteCard(WriteIDMode, writer, card);
            }
            writer.WriteLine();
        }
        private void WriteCard(bool WriteIDMode, StreamWriter writer, DeckCard card)
        {
            string cardFormat = "{0,-30}{1,-5} *{2}";

            if (WriteIDMode)
            {
                CardInfo info = CardDatabase.FindCardByID(card.ID);
                string pt = null;
                if (info.Power != null && info.Power.Trim() != "")
                    pt = string.Format(" {0}/{1}", info.Power, info.Toughness);

                writer.WriteLine(string.Format("# {0} x {1} ({2}) {3} {4}{5}", card.Quantity, card.Name, card.Set, info.Mana, info.Type, pt));
                for (int i = 0; i < card.Quantity; i++)
                {
                    writer.WriteLine(card.ID);
                }
                writer.WriteLine();
            }
            else
            {
                writer.WriteLine(string.Format(cardFormat, card.Name, "(" + card.Set + ")", card.Quantity));
            }
        }

        internal static void WriteDeckInIDFormat(Stream stream, Deck deck)
        {
            var writer = Util.GetWriter(stream);

            writer.WriteLine(string.Format("#NAME:{0}", deck.Name));
            
            if (deck.Description != null && deck.Description.Trim() != "")
                writer.WriteLine(string.Format("#DESC:{0}", deck.Description));

            List<DeckCard> unkownCards = new List<DeckCard>();

            foreach (var card in deck.Cards)
            {
                if (card.ID <= 0)
                {
                    //unknown card
                    unkownCards.Add(card);
                }
                else
                {
                    for (int i = 0; i < card.Quantity; i++)
                        writer.WriteLine(card.ID);
                }
            }

            writer.Flush();
        }
    }
}
