﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Csv;
using Wagic.CardDatabase;
using Wagic.Decks;
using System.IO;
using Wagic.Profiles;
using Wagic.Translation;

namespace Wagic.Profiles
{
    public class MTGOProfileExport : IProfileExport
    {
        //data should look like this
        //Card Name,Online,For Trade,Rarity,Set,No.,Premium
        //Event Ticket,2,0,EVENT, ,No
        //Barony Vampire,2,0,C,M11,82/249,No
        //Bloodthrone Vampire,2,0,C,M11,85/249,No

        public void Export(Profile profile, ICardDatabase cardDatabase, ISetNameTranslator setNameTranslator, string targetFileName)
        {
            Decks.Deck collection = profile.GetCollection(cardDatabase);

            var export = collection.Cards.Select(x =>
                {
                    var info = cardDatabase.FindCardByID(x.ID);
                    return new MTGOCsvRecord()
                    {
                        CardName = x.Name,
                        Rarity = info.Rarity,
                        ForTrade = "0",
                        Number = "",
                        Set = x.Set,
                        Online = x.Quantity.ToString(),
                        Premium = "No"
                    };
                });
            
            using(Stream s = Util.OpenWrite(targetFileName))
            {
                CsvWriter writer = new CsvWriter(s);
                CsvUtil.WriteToCsv<MTGOCsvRecord>(export, writer);
            }
        }

        public class MTGOCsvRecord
        {
            [CsvField("Card Name")]
            public string CardName { get; set; }
            [CsvField("Online")]
            public string Online { get; set; }
            [CsvField("For Trade")]
            public string ForTrade { get; set; }
            [CsvField("Rarity")]
            public string Rarity { get; set; }
            [CsvField("Set")]
            public string Set { get; set; }
            [CsvField("No.")]
            public string Number { get; set; }
            [CsvField("Premium")]
            public string Premium { get; set; }
        }
    }
}
