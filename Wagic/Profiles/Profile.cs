﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Wagic.CardDatabase;
using System.IO;
using Wagic.Decks;
using Wagic.Translation;
using System.Text.RegularExpressions;

namespace Wagic.Profiles
{
    public class Profile
    {
        public const string UNLOCK_REGEX = @"unlocked_(?<set>\w+)=(?<date>\d{4}/\d{1,2}/\d{1,2}\@\d{1,2}\:\d{1,2})\ ?V?";

        public string Name { get; private set; }
        public string ProfileRoot { get; private set; }
        public int Money { get; private set; }
                
        public IList<UnlockedSet> UnlockedSets { get; private set; }


        public static IList<Profile> GetAllProfiles(string wagicRootDirectory)
        {
            List<Profile> results = new List<Profile>();

            var defaultProfile = GetDefaultProfile(wagicRootDirectory);
            if (defaultProfile != null)
            {
                results.Add(defaultProfile);
            }

            results.AddRange(GetUserProfiles(wagicRootDirectory));
            
            return results;
        }

        public static Profile GetDefaultProfile(string wagicRootDirectory)
        {
            //look for default profile
            string defaultProfilePath = WagicPaths.GetDefaultProfile(wagicRootDirectory);

            if (Directory.Exists(defaultProfilePath))
                return new Profile(defaultProfilePath);
            else
                return null;
        }
        public static IList<Profile> GetUserProfiles(string wagicRootDirectory)
        {
            string profilesFolderPath = WagicPaths.GetProfilesFolder(wagicRootDirectory);

            List<Profile> results = new List<Profile>();
            foreach (var dir in Directory.GetDirectories(profilesFolderPath, "*", SearchOption.TopDirectoryOnly))
            {
                results.Add(new Profile(dir));
            }
            return results;
        }

        public Profile(string profileRootDir)
        {
            ProfileRoot = profileRootDir;
            Name = Path.GetFileNameWithoutExtension(profileRootDir);

            var dataFile = WagicPaths.GetProfileDataFile(ProfileRoot);
            Money = ReadProfileMoney(dataFile);

            var optionsFile = WagicPaths.GetProfileOptionsFile(ProfileRoot);
            UnlockedSets = ReadUnlockSets(optionsFile);
        }

        public Deck GetCollection(ICardDatabase cardDatabase)
        {
            //collection is in collection.dat
            DirectoryInfo profileDir = new DirectoryInfo(ProfileRoot);

            var collectionFile = new FileInfo(WagicPaths.GetProfileCollectionFile(ProfileRoot));

            if (collectionFile.Exists)
            {
                Wagic.Decks.Formats.WagicDeckFormat wagicFormat = new Decks.Formats.WagicDeckFormat(cardDatabase);
                Deck deck = null;

                using(Stream s = collectionFile.OpenRead())
                {
                    deck = wagicFormat.ReadDeck(s);
                }

                return deck;
            }
            else
            {
                return null;
            }
        }
        public IList<Deck> GetDecks(ICardDatabase cardDatabase)
        {
            var decks = WagicPaths.GetProfileDecks(this.ProfileRoot);
            Wagic.Decks.Formats.WagicDeckFormat deckFormat = new Decks.Formats.WagicDeckFormat(cardDatabase);

            return decks.Select(x =>
                {
                    using (Stream s = Util.OpenRead(x))
                    {
                        return deckFormat.ReadDeck(s);
                    }

                }).ToList();
        }

        public void AddMoneyAmount(int amount)
        {
            Money += amount;
            SetMoneyAmount(this.Money);
        }
        public void SetMoneyAmount(int amount)
        {
            var dataFile = WagicPaths.GetProfileDataFile(ProfileRoot);
            WriteProfileMoney(dataFile, amount);

            this.Money = amount;
        }

        public void UnlockSets(string set)
        {
            var optionsFile = WagicPaths.GetProfileOptionsFile(ProfileRoot);
            
        }
        public void UnlockSets(IEnumerable<string> sets)
        {
            var optionsFile = WagicPaths.GetProfileOptionsFile(ProfileRoot);
            WriteUnlockSets(optionsFile, sets.ToList());
        }

        #region File IO Methods

        private static int ReadProfileMoney(string fileName)
        {
            if (File.Exists(fileName))
            {
                string[] lines = Util.ReadAllLines(fileName);
                return int.Parse(lines[0]);
            }
            else
            {
                return 0;
            }
        }
        private static void WriteProfileMoney(string fileName, int amount)
        {
            string[] lines = new string[1];

            if (File.Exists(fileName))
            {
                lines = Util.ReadAllLines(fileName);
            }

            lines[0] = amount.ToString();

            Util.WriteAllLines(fileName, lines);
        }

        private static IList<UnlockedSet> ReadUnlockSets(string fileName)
        {
            var lines = Util.ReadAllLines(fileName);
            return GetUnlocksFromFileContents(lines);
        }
        private static IList<UnlockedSet> GetUnlocksFromFileContents(string[] lines)
        {
            List<UnlockedSet> results = new List<UnlockedSet>();

            foreach (var item in lines)
            {
                var m = Regex.Match(item, UNLOCK_REGEX);

                if (m.Success)
                {
                    UnlockedSet us = new UnlockedSet();
                    us.SetName = m.Groups["set"].Value;
                    us.UnlockedDate = DateTime.Parse(m.Groups["date"].Value.Replace("@", " "));

                    results.Add(us);
                }
            }

            return results;
        }
        private static void WriteUnlockSets(string fileName, List<string> setNames)
        {
            var lines = Util.ReadAllLines(fileName);
            var unlockedSets = GetUnlocksFromFileContents(lines).Select(x => x.SetName).ToList();
            var newLines = lines.ToList();

            DateTime unlockDate = DateTime.Now;

            foreach (var setName in setNames)
            {
                //unlocked_EVT=2012/6/21@22:52 V
                string unlockEntry = string.Format("unlocked_{0}={1}@{2} V", setName, unlockDate.ToString("yyyy/M/d"), unlockDate.ToString("HH:mm"));

                if (unlockedSets.Contains(setName))
                    continue;

                //get last index tha matchs the regex
                int index = 0;
                for (int i = 0; i < newLines.Count; i++)
                {
                    if (Regex.IsMatch(newLines[i], UNLOCK_REGEX))
                        index = i;
                }

                newLines.Insert(index + 1, unlockEntry);
            }

            Util.WriteAllLines(fileName, newLines.ToArray());
        }

        #endregion

        public override string ToString()
        {
            return "Profile: " + Name;
        }
    }
}
