﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wagic.Profiles
{
    public class UnlockedSet
    {
        public string SetName { get; set; }
        public DateTime UnlockedDate { get; set; }
    }
}
