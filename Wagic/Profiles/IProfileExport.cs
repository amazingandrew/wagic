﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Wagic.CardDatabase;
using Wagic.Decks;
using Wagic.Translation;

namespace Wagic.Profiles
{
    public interface IProfileExport
    {
        void Export(Profile profile, ICardDatabase cardDatabase, ISetNameTranslator setNameTranslator, string targetFileName);
    }
}
