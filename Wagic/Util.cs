﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Wagic
{
    internal class Util
    {
        public static Stream OpenRead(string fileName)
        {
            Stream s = File.Open(fileName, FileMode.Open, FileAccess.Read);
            return s;
        }
        public static Stream OpenWrite(string fileName)
        {
            Stream s = File.Open(fileName, FileMode.Create, FileAccess.Write);
            return s;
        }

        public static StreamReader GetReader(string fileName)
        {
            return GetReader(OpenRead(fileName));
        }
        public static StreamReader GetReader(Stream stream)
        {
            return new StreamReader(stream);
        }

        public static StreamWriter GetWriter(string fileName)
        {
            return GetWriter(OpenWrite(fileName));
        }
        public static StreamWriter GetWriter(Stream stream)
        {
            StreamWriter writer = new StreamWriter(stream);
            writer.NewLine = "\n";

            return writer;
        }

        public static string[] ReadAllLines(string fileName)
        {
            return File.ReadAllLines(fileName);
        }
        public static void WriteAllLines(string fileName, string[] lines)
        {
            using (var writer = GetWriter(fileName))
            {
                foreach (var item in lines)
                {
                    writer.WriteLine(item);
                }

                writer.Flush();
            }
        }
    }
}
