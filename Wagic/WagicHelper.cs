﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Wagic.CardDatabase;
using Wagic.Profiles;
using Wagic.Decks;
using Wagic.Translation;

namespace Wagic
{
    public class WagicHelper
    {
        public string WagicRoot { get; private set; }
        
        public ICardDatabase DB { get; private set; }

        public WagicHelper(string wagicRoot)
        {
            if (!WagicPaths.IsValidWagicRoot(wagicRoot))
                throw new WagicException(string.Format("{0} is not a valid wagic root path."));

            WagicRoot = wagicRoot;
            DB = new WagicCardDatabase(wagicRoot);
        }
        
        //Cards
        public IList<CardSet> GetWagicCardDatabase()
        {
            return DB.GetAllCards();
        }
        public CardInfo FindCardById(long id)
        {
            return DB.FindCardByID(id);
        }
        public CardInfo FindCard(string cardName)
        {
            return DB.FindCard(cardName);
        }
        public CardInfo FindCard(string setName, string cardName)
        {
            return DB.FindCard(setName, cardName);
        }

        //Profiles
        public IList<Profile> GetAllProfiles()
        {
            return Profiles.Profile.GetAllProfiles(WagicRoot);
        }
        public IList<Profile> GetUserProfiles()
        {
            return Profiles.Profile.GetUserProfiles(WagicRoot);
        }
        public Profile GetDefaultProfile()
        {
            return Profile.GetDefaultProfile(WagicRoot);
        }
        public Deck GetAllProfiles(Profile profile)
        {
            return profile.GetCollection(DB);
        }
        public IList<Deck> GetProfileDecks(Profile profile)
        {
            return profile.GetDecks(this.DB);
        }
        public Deck GetProfileCollection(Profile profile)
        {
            return profile.GetCollection(DB);
        }
        public void WriteProfileCollection(Profile profile, Deck newCollection)
        {
            var collectionFile = WagicPaths.GetProfileCollectionFile(profile.ProfileRoot);
            using (Stream s = Util.OpenWrite(collectionFile))
                Wagic.Decks.Formats.WagicDeckFormat.WriteDeckInIDFormat(s, newCollection);
        }
        public void AddMoneyToProfile(Profile profile, int amountToAdd)
        {
            profile.AddMoneyAmount(amountToAdd);
        }
        public void SetMoneyToProfile(Profile profile, int amount)
        {
            profile.SetMoneyAmount(amount);
        }
        
        public void UnlockProfileCardSet(Profile profile, string set)
        {
            UnlockProfileCardSet(profile, new List<string>() { set });
        }
        public void UnlockProfileCardSet(Profile profile, List<string> sets)
        {
            var options = Wagic.WagicPaths.GetProfileOptionsFile(profile.ProfileRoot);
            profile.UnlockSets(sets);
        }

        //Decks
        public Deck LoadDeck(string fileName)
        {
            if (fileName == null)
                return null;

            Wagic.Decks.Formats.IDeckFormat deckFormat = null;

            Deck deck = null;
            using (Stream s = Util.OpenRead(fileName))
                deck = deckFormat.ReadDeck(s);
            return deck;            
        }
        public void SaveDeck(Deck deck, string targetFileName)
        {
            Wagic.Decks.Formats.WagicDeckFormat wdf = new Decks.Formats.WagicDeckFormat(DB);

            using (Stream s = Util.OpenWrite(targetFileName))
                wdf.WriteDeck(s, deck);
        }
        public string GetNewProfileDeckName(Profile profile)
        {
            var deckPath = WagicPaths.GetNextAvailableDeckName(profile.ProfileRoot);
            return deckPath;
        }
        public void SaveNewDeckToProfile(Profile profile, Deck newDeck)
        {
            var deckPath = WagicPaths.GetNextAvailableDeckName(profile.ProfileRoot);
            this.SaveDeck(newDeck, deckPath);
        }
    }
}
