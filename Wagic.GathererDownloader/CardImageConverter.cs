﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GathererDownloaderData;
using Wagic.Translation;
using Wagic.CardDatabase;
using Ionic.Zip;
using System.IO;


namespace Wagic.GathererDownloader
{
    public class CardImageConverter
    {
        public void BuildWagicCardImagesIntoFolder(IList<CardInfo> cardsToLookFor, GathererDownloaderData.CardDatabase cardDatabse, ISetNameTranslator setTranslator, CardImageLocator imgLocator, string targetFolder, int fullLongCardSize, int thumbLongCardSize, bool overwrite)
        {
            List<CardRecord> processed = new List<CardRecord>();

            foreach (var wagicCard in cardsToLookFor)
            {
                GathererDownloaderData.CardRecord match = null;

                var possibleCards = cardDatabse.Cards.Where(x => x.Name.ToLower().Trim() == wagicCard.Name.ToLower().Trim()).ToList();

                if (possibleCards.Count == 0)
                    continue;

                foreach (var pc in possibleCards)
                {
                    if (processed.Contains(pc))
                        match = pc; //set for last one, but keep going just in case

                    var properSetName = setTranslator.FindProperSetName(pc.Set);
                    if (properSetName.ToLower().Trim() == wagicCard.Set.ToLower().Trim())
                    {
                        //the correct match!
                        match = pc;
                        break;
                    }
                }

                //no match found
                //get first or last
                if (match == null)
                    continue;



                //add completed
                processed.Add(match);

                var cardImagePath = imgLocator.LocateImage(match);

                if (cardImagePath == null)
                    continue;

                string imageFolderPath = Path.Combine(targetFolder, wagicCard.Set);
                string thumbsFolderPath = Path.Combine(imageFolderPath, "thumbnails");

                string newCardPath = Path.Combine(imageFolderPath, wagicCard.ID + ".jpg");
                string thumbCardPath = Path.Combine(thumbsFolderPath, wagicCard.ID + ".jpg");

                if (!Directory.Exists(imageFolderPath))
                    Directory.CreateDirectory(imageFolderPath);
                if (!Directory.Exists(thumbsFolderPath))
                    Directory.CreateDirectory(thumbsFolderPath);

                if (!File.Exists(newCardPath) || overwrite)
                    CardImageResizer.ResizeImageLongEdge(cardImagePath, newCardPath, fullLongCardSize);

                if (!File.Exists(thumbCardPath) || overwrite)
                    CardImageResizer.ResizeImageLongEdge(cardImagePath, thumbCardPath, thumbLongCardSize);
            }
        }
        
        public void ZipUpFolders(string rootFolder)
        {
            DirectoryInfo dir = new DirectoryInfo(rootFolder);

            var subs = dir.GetDirectories();

            foreach (var subDir in subs)
            {
                string zipFilePath = Path.ChangeExtension(subDir.FullName, "zip");

                if (File.Exists(zipFilePath))
                    File.Delete(zipFilePath);

                ZipFile zip = new ZipFile(zipFilePath);

                var files = subDir.GetFiles("*", SearchOption.AllDirectories).Select(x => x.FullName);

                foreach (var f in files)
                {
                    string targetPathInZipFile = f.Remove(0, subDir.FullName.Length);
                    targetPathInZipFile = Path.GetDirectoryName(targetPathInZipFile);
                    zip.AddFile(f, targetPathInZipFile);
                }

                zip.Save();                
            }
        }
        
        public void MoveZipFilesIntoWagic(string rootFolder, string wagicRoot)
        {
            var zipFiles = Directory.GetFiles(rootFolder, "*.zip");

            foreach (var item in zipFiles)
            {
                string setPath = Wagic.WagicPaths.GetUserSetFolder(wagicRoot, Path.GetFileNameWithoutExtension(item));
                string targetPath = Path.Combine(setPath, Path.GetFileName(item));
                string backupPath = Path.ChangeExtension(targetPath, ".zip.bak");

                if (!Directory.Exists(setPath))
                    Directory.CreateDirectory(setPath);

                //if file already exists, delete it??
                if (File.Exists(targetPath) && !File.Exists(backupPath))
                    File.Move(targetPath, backupPath);

                if (File.Exists(targetPath))
                    File.Delete(targetPath);


                File.Move(item, targetPath);
	        }
        }
    }
}
