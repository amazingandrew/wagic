﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Wagic.Decks;
using System.Configuration;
using Wagic.CardDatabase;
using Wagic.Translation;
using Wagic;
using Wagic.Profiles;
using System.Diagnostics;
using Wagic.Decks.MagicWorkstation;

namespace WagicDeckConverter
{
    public partial class MainForm : Form
    {
        public static WagicHelper Wagic { get; private set; }
        public static SetNameTranslator SetTranslator { get; private set; }


        public MainForm()
        {
            InitializeComponent();

            string wagicRoot = ConfigurationManager.AppSettings["WAGIC_ROOT"];

            if (wagicRoot == null || wagicRoot.Trim() == "" || !Directory.Exists(wagicRoot))
            {
                MessageBox.Show("Invalid wagic root setting, please set the correct value in the app.config file" +
                    "\r\n\r\nconfiguration file path:\r\n" +
                    ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).FilePath,
                    this.Text + " - Invalid Configuration");
                Environment.Exit(-1);
            }

            if (!WagicPaths.IsValidWagicRoot(wagicRoot))
            {
                MessageBox.Show(wagicRoot + " does not look like a valid wagic root" +
                    "\r\nplease set the correct value in the app.config file" +
                    "\r\n\r\nconfiguration file path:\r\n" +
                    ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).FilePath,
                    this.Text + " - Invalid Configuration");

            }

            Wagic = new WagicHelper(wagicRoot);
            SetTranslator = new SetNameTranslator(Wagic.DB, "Resources\\sets.txt");

            tbWagicProfile.DataSource = Wagic.GetAllProfiles();
            tbWagicProfile.DisplayMember = "Name";
        }

        private void btnBrowseSource_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Magic Workstation Deck|*.mwDeck";

            if (tbSource.Text.Trim() != "")
                dlg.InitialDirectory = Path.GetDirectoryName(tbSource.Text);


            var result = dlg.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                tbSource.Text = dlg.FileName;
            }
        }
        
        private void btnConvert_Click(object sender, EventArgs e)
        {
            var selectedProfile = tbWagicProfile.SelectedValue as Profile;

            if (selectedProfile != null)
            {
                string newDeckName = Wagic.GetNewProfileDeckName(selectedProfile);
                RunConversion(tbSource.Text, newDeckName);
                MessageBox.Show("Saved new deck at " + newDeckName);
                Process.Start(newDeckName);
            }
        }

        private void RunConversion(string source, string target)
        {
            Deck deck = null;

            IDeckImport mwImport = new MagicWorkstationImport(Wagic.DB, SetTranslator, false);
            using(Stream s = File.OpenRead(source))
                deck = mwImport.Import(s);

            deck.Name = Path.GetFileNameWithoutExtension(source);
            Wagic.SaveDeck(deck, target);
        }

        private void btnConvertFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dlg = new FolderBrowserDialog();
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                var dir = new DirectoryInfo(dlg.SelectedPath);
                var files = dir.GetFiles("*.mwdeck", SearchOption.AllDirectories);

                foreach (var f in files)
                {
                    var target = Path.ChangeExtension(f.FullName, ".txt");
                    RunConversion(f.FullName, target);
                }
            }

            MessageBox.Show("completed!");
        }
    }
}
