﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace GathererDownloaderData
{
    public class CardImageLocator
    {
        public string CardImagesRoot { get; private set; }

        private Dictionary<long, string> CROP_FilesLookup { get; set; }
        private Dictionary<long, string> FULL_FilesLookup { get; set; }

        private Dictionary<long, string> FilesLookup { get; set; }

        public const string DefaultCropsFolderName = "Crops";
        public const string DefaultFullsFolderName = "Fulls";
        
        public CardImageLocator(string cardImagesRoot) 
            : this(cardImagesRoot, DefaultFullsFolderName, DefaultCropsFolderName)
        {
            
        }

        public CardImageLocator(string cardImagesRoot, string fullsFolderName, string croppedFolderName)
        {
            if (cardImagesRoot == null)
                throw new ArgumentNullException("cardImagesRoot");
            if (cardImagesRoot.Trim() == string.Empty)
                throw new ArgumentException("cardImagesRoot");

            CardImagesRoot = cardImagesRoot;

            string cropsFolderPath = Path.Combine(cardImagesRoot, croppedFolderName);
            string fullsFolderPath = Path.Combine(cardImagesRoot, fullsFolderName);

            if (Directory.Exists(cropsFolderPath))
                CROP_FilesLookup = BuildDictionary(cropsFolderPath);
            if (Directory.Exists(fullsFolderPath))
                FULL_FilesLookup = BuildDictionary(fullsFolderPath);
            else
            {
                FilesLookup = BuildDictionary(cardImagesRoot);
            }
        }

        private Dictionary<long, string> BuildDictionary(string folderRoot)
        {
            Dictionary<long, string> dic = new Dictionary<long, string>();

            var selectedFiles = Directory.GetFiles(folderRoot, "*.jpg", SearchOption.AllDirectories);
            foreach (var fileName in selectedFiles)
            {
                string nameWithoutExtension = Path.GetFileNameWithoutExtension(fileName);
                long l = 0;
                if (long.TryParse(nameWithoutExtension, out l))
                    dic.Add(l, fileName);
            }

            return dic;
        }

        public string LocateImage(CardRecord card)
        {
            if (FilesLookup != null && FilesLookup.ContainsKey(card.ID))
                return FilesLookup[card.ID];

            if (FULL_FilesLookup != null && FULL_FilesLookup.ContainsKey(card.ID))
                return FULL_FilesLookup[card.ID];

            if (CROP_FilesLookup != null && CROP_FilesLookup.ContainsKey(card.ID))
                return CROP_FilesLookup[card.ID];
            
            return null;
        }

        public string LocateFullImage(CardRecord card)
        {
            if (FULL_FilesLookup != null && FULL_FilesLookup.ContainsKey(card.ID))
                return FULL_FilesLookup[card.ID];

            return null;
        }
        public string LocateCropImage(CardRecord card)
        {
            if (CROP_FilesLookup != null && CROP_FilesLookup.ContainsKey(card.ID))
                return CROP_FilesLookup[card.ID];

            return null;
        }
    }
}
