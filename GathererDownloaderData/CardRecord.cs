﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace GathererDownloaderData
{
    public class CardRecord
    {
        [XmlElement("id")]
        public long ID { get; set; }
        [XmlElement("lang")]
        public string Lang { get; set; }
        [XmlElement("name")]
        public string Name { get; set; }
        [XmlElement("altart")]
        public string AltArt { get; set; }
        [XmlElement("cost")]
        public string Cost { get; set; }
        [XmlElement("color")]
        public string Color { get; set; }
        [XmlElement("type")]
        public string Type { get; set; }
        [XmlElement("set")]
        public string Set { get; set; }
        [XmlElement("rarity")]
        public string Rarity { get; set; }
        [XmlElement("power")]
        public string Power { get; set; }
        [XmlElement("toughness")]
        public string Toughness { get; set; }
        [XmlElement("rules")]
        public string Rules { get; set; }
        [XmlElement("printedname")]
        public string PrintedName { get; set; }
        [XmlElement("printedtype")]
        public string PrintedType { get; set; }
        [XmlElement("printedrules")]
        public string PrintedRules { get; set; }
        [XmlElement("flavor")]
        public string Flavor { get; set; }
        [XmlElement("watermark")]
        public string Watermark { get; set; }
        [XmlElement("cardnum")]
        public string CardNum { get; set; }
        [XmlElement("artist")]
        public string Artist { get; set; }
        [XmlElement("sets")]
        public string Sets { get; set; }
        [XmlElement("rulings")]
        public string Rulings { get; set; }

    }
}
