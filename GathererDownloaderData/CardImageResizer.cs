﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;

namespace GathererDownloaderData
{
    public class CardImageResizer
    {
        public static void ResizeImage(string sourceImg, string targetImg, int x, int y)
        {
            var originalImage = Image.FromFile(sourceImg);

            float sourceX = originalImage.Width;
            float sourceY = originalImage.Height;

            float targetX = x;
            float targetY = y;

            Bitmap resizedImage = new Bitmap((int)Math.Round(targetX), (int)Math.Round(targetY));

            Graphics g = Graphics.FromImage(resizedImage);

            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            g.SmoothingMode = SmoothingMode.HighQuality;
            g.PixelOffsetMode = PixelOffsetMode.HighQuality;
            g.CompositingQuality = CompositingQuality.HighQuality;

            g.DrawImage(originalImage, 0, 0, targetX, targetY);

            resizedImage.Save(targetImg, originalImage.RawFormat);

            g.Dispose();
            originalImage.Dispose();
            resizedImage.Dispose();
        }
        public static void ResizeImageLongEdge(string sourceImg, string targetImg, int targetLongEdgeSize)
        {
            var originalImage = Image.FromFile(sourceImg);

            float sourceX = originalImage.Width;
            float sourceY = originalImage.Height;

            float targetX = 0;
            float targetY = 0;

            if (sourceX > sourceY)
            {
                targetX = targetLongEdgeSize;
                targetY = sourceY * (targetLongEdgeSize / sourceX);
            }
            else
            {
                targetX = sourceX * (targetLongEdgeSize / sourceY); ;
                targetY = targetLongEdgeSize; 
            }

            Bitmap resizedImage = new Bitmap((int)Math.Round(targetX), (int)Math.Round(targetY));

            Graphics g = Graphics.FromImage(resizedImage);

            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            g.SmoothingMode = SmoothingMode.HighQuality;
            g.PixelOffsetMode = PixelOffsetMode.HighQuality;
            g.CompositingQuality = CompositingQuality.HighQuality;

            g.DrawImage(originalImage, 0,0,targetX, targetY);
            
            resizedImage.Save(targetImg, originalImage.RawFormat);

            g.Dispose();
            originalImage.Dispose();
            resizedImage.Dispose();
        }
    }
}
