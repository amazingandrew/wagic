﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Xml;

namespace GathererDownloaderData
{
    [Serializable]
    [XmlRoot(XmlElement)]
    public class CardDatabase
    {
        public const string XmlElement = "mtg_card_information";
        public const string XmlNamespace = "http://ark42.com/mtg/";

        [XmlArray("cards")]
        [XmlArrayItem("card")]
        public List<CardRecord> Cards { get; set; }

        public CardDatabase()
        {
            Cards = new List<CardRecord>();
        }

        public void SaveToFile(string fileName)
        {
            WriteToFile(fileName, this);
        }

        public static CardDatabase ReadFromFile(string fileName)
        {
            System.Xml.Serialization.XmlSerializer xml =
                new XmlSerializer(typeof(CardDatabase));

            System.Xml.Serialization.XmlSerializerNamespaces namespaces =
                new XmlSerializerNamespaces();
            namespaces.Add(XmlElement, XmlNamespace);        
    
            using (Stream s = File.OpenRead(fileName))
            {
                return (CardDatabase)xml.Deserialize(s);
            }
        }
        public static void WriteToFile(string fileName, CardDatabase db)
        {
            System.Xml.Serialization.XmlSerializer xml =
                new XmlSerializer(typeof(CardDatabase));

            System.Xml.Serialization.XmlSerializerNamespaces namespaces =
                new XmlSerializerNamespaces();
            namespaces.Add(XmlElement, XmlNamespace);            

            using (Stream s = File.Open(fileName, FileMode.Create, FileAccess.Write))
            {
                XmlWriter writer = XmlWriter.Create(s,
                    new XmlWriterSettings()
                    {
                        Indent = true, IndentChars= "\t"
                    });

                xml.Serialize(writer, db, namespaces);
                s.Flush();
            }
        }
    }
}
