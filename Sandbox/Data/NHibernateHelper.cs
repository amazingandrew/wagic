﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate;
using NHibernate.Cfg;
using NHibernate;

namespace Sandbox.Data
{/// <summary>
    /// A static utility class that can obtain Nhibernate SessionFactory and Configuration.
    /// </summary>
    public static class NHibernateHelper
    {
        public static Configuration Configuration { get; private set; }
        public static ISessionFactory SessionFactory { get; private set; }

        /// <summary>
        /// Initializes the <see cref="NHibernateHelper"/> class.
        /// </summary>
        static NHibernateHelper()
        {

        }

        /// <summary>
        /// Builds the session factory.
        /// </summary>
        public static void BuildSessionFactory()
        {
            BuildSessionFactory(null);
        }

        /// <summary>
        /// Builds the session factory.
        /// </summary>
        /// <returns></returns>
        public static void BuildSessionFactory(Action<Configuration> extraConfiguration)
        {
            Configuration = new Configuration();
            Configuration.Configure();

            PersistenceModel model = BuildPersistanceModel();
            model.Configure(Configuration);
            Configuration.AddAssembly(typeof(NHibernateHelper).Assembly);

            if (extraConfiguration != null)
                extraConfiguration(Configuration);

            SessionFactory = Configuration.BuildSessionFactory();
        }

        /// <summary>
        /// Builds the persistance model.
        /// </summary>
        /// <returns></returns>
        public static PersistenceModel BuildPersistanceModel()
        {
            PersistenceModel model = new PersistenceModel();
            model.AddMappingsFromAssembly(typeof(NHibernateHelper).Assembly);
            model.Conventions.AddAssembly(typeof(NHibernateHelper).Assembly);
            return model;
        }

    }
}
