﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Wagic.CardDatabase;

namespace Sandbox.Data
{
    public class CardDataDTO
    {
        public virtual string Set { get; set; }

        public virtual long ID { get; set; }
        public virtual string Rarity { get; set; }
        public virtual string PrimitiveName { get; set; }

        public virtual string Name { get; set; }

        public virtual string Type { get; set; }
        public virtual string Text { get; set; }
        public virtual string Mana { get; set; }
        public virtual string Power { get; set; }
        public virtual string Toughness { get; set; }


        public virtual IList<string> Auto { get;  set; }
        public virtual IList<string> AutoGraveyard { get;  set; }
        public virtual IList<string> Abilities { get;  set; }
        public virtual IList<string> Subtypes { get;  set; }
        public virtual IList<string> Color { get;  set; }

        public CardDataDTO()
        {
            Auto = new List<string>();
            AutoGraveyard = new List<string>();
            Abilities = new List<string>();
            Subtypes = new List<string>();
            Color = new List<string>();
        }

        public static CardDataDTO FromCard(CardInfo c)
        {
            CardDataDTO d = new CardDataDTO();
            d.Abilities = c.Abilities;
            d.Auto = c.Auto;
            d.AutoGraveyard = c.AutoGraveyard;
            d.Color = c.Color;
            d.ID = c.ID;
            d.Mana = c.Mana;
            d.Name = c.Name;
            d.Power = c.Power;
            d.PrimitiveName = c.PrimitiveName;
            d.Rarity = c.Rarity;
            d.Set = c.Set;
            d.Subtypes = c.Subtypes;
            d.Text = c.Text;
            d.Toughness = c.Toughness;
            d.Type = c.Type;
            return d;
        }
    }
}
