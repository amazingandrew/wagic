﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Wagic.CardDatabase;
using FluentNHibernate.Mapping;

namespace Sandbox.Data
{
    public class CardDataMap : ClassMap<CardDataDTO>
    {
        public CardDataMap()
        {
            Table("cards");

            Id(x => x.ID)
                .GeneratedBy.Assigned();

            Map(x => x.Mana);
            Map(x => x.Name);
            Map(x => x.Power);
            Map(x => x.PrimitiveName);
            Map(x => x.Rarity);
            Map(x => x.Set).Column("CardSet");
            Map(x => x.Text);
            Map(x => x.Toughness);
            Map(x => x.Type);

            HasMany(x => x.Abilities)
                .KeyColumn("id")
                .Element("value")
                .Table("card_abilities");
            HasMany(x => x.Auto)
                .KeyColumn("id")
                .Element("value")
                .Table("card_autos");
            HasMany(x => x.AutoGraveyard)
                .KeyColumn("id")
                .Element("value")
                .Table("card_autograveyard");
            HasMany(x => x.Color)
                .KeyColumn("id")
                .Element("value")
                .Table("card_colors");
            HasMany(x => x.Subtypes)
                .KeyColumn("id")
                .Element("value")
                .Table("card_subtypes");
        }
    }
}
