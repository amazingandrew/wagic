﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Wagic.CardDatabase;
using Sandbox.Data;
using NHibernate;
using Wagic.Decks;
using Wagic;
using Wagic.Profiles;
using GathererDownloaderData;
using Wagic.Translation;
using System.Drawing;

namespace Sandbox
{
    class Program
    {
        public const string PSPWagicRoot = @"K:\PSP\GAME\WTH";
        public const string WagicRoot = @"C:\Users\Admin\Desktop\wagic\Wagic0186";
        public const string ExportTestFile = @"C:\users\admin\desktop\export-mtgo.csv";

        public const string GathererTestFile = @"C:\users\admin\desktop\SampleDB.xml";
        public const string GathererTestWrite = @"C:\users\admin\desktop\SampleDB-Write.xml";
        public const string GathererImagePath = @"C:\Users\Admin\Desktop\wagic\CardImages";

        static void Main(string[] args)
        {
            var db = CardDatabase.ReadFromFile(@"C:\Users\Admin\Desktop\wagic\Tools\GathererDownloader-Master.xml");
            CardImageLocator imgLocator = new CardImageLocator(GathererImagePath);
            
            WagicHelper wagic = new WagicHelper(WagicRoot);
            SetNameTranslator setTranslator = new SetNameTranslator(wagic.DB, "Resources/sets.txt");


            var cardsToLook = wagic.DB.GetAllCards().SelectMany(x => x.Cards).Where(x => x.Set.Contains("5ED")).ToList();

            string TempWagicCardImagesPath = Path.GetTempPath() + "\\CardImages";

            Wagic.GathererDownloader.CardImageConverter imgConverter = new Wagic.GathererDownloader.CardImageConverter();
            //imgConverter.BuildWagicCardImagesIntoFolder(cardsToLook, db, setTranslator, imgLocator, TempWagicCardImagesPath, 680, 250, false);
            imgConverter.ZipUpFolders(TempWagicCardImagesPath);
            imgConverter.MoveZipFilesIntoWagic(TempWagicCardImagesPath, wagic.WagicRoot);
            

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Press enter to close this program...");
            Console.ReadLine();
        }

        private static void RebuildProfileDecks(Wagic.WagicHelper wagic, Profile profile)
        {
            var decks = wagic.GetProfileDecks(profile);

            var deckFiles = Directory.GetFiles(profile.ProfileRoot, "deck*");
            foreach (var file in deckFiles)
            {
                File.Delete(file);
            }

            foreach (var deck in decks)
            {
                wagic.SaveNewDeckToProfile(profile, deck);
            }
        }
        private static void GiveEveryCard(Wagic.WagicHelper wagic, Profile profile)
        {
            //give 20 of every card
            var allCards = wagic.DB.GetAllCards().OrderByDescending(x =>
            {
                int z = 0;
                int.TryParse(x.Name, out z);
                return z;
            }).SelectMany(x => x.Cards).ToList();

            var names = allCards.Select(x => x.Name).Distinct();
            List<CardInfo> list = new List<CardInfo>();

            foreach (var item in names)
            {
                list.Add(allCards.Where(x => x.Name == item).First());
            }

            var cards = list.Select(x =>
            {
                DeckCard dc = new DeckCard();
                dc.ID = x.ID;
                dc.Name = x.Name;
                dc.Set = x.Set;
                dc.Quantity = 4;
                dc.IsSideboard = false;
                return dc;
            });

            Deck newCollection = new Deck();
            newCollection.Name = "collection";
            foreach (var c in cards)
            {
                newCollection.AddToDeck(c);
            }


            //give 20 of every card
            wagic.WriteProfileCollection(profile, newCollection);
            //unlock all sets
            wagic.UnlockProfileCardSet(profile, wagic.DB.GetAllCards().Select(x => x.Name).ToList());
        }
        public static void AddMissingDeckCardsToCollection(WagicHelper wagic, Profile profile)
        {
            var collection = wagic.GetProfileCollection(profile);
            bool changed = false;

            foreach (var deck in wagic.GetProfileDecks(profile))
            {
                foreach (var card in deck.Cards)
                {
                    var cardCollection = collection.Cards.Where(x => x.ID == card.ID).FirstOrDefault();

                    if (cardCollection == null)
                    {
                        collection.AddToDeck(card);
                        changed = true;
                    }
                    else
                    {
                        if (cardCollection.Quantity < card.Quantity)
                        {
                            cardCollection.Quantity = card.Quantity;
                            changed = true;
                        }
                    }
                }
            }

            if (changed)
                wagic.WriteProfileCollection(profile, collection);            


            //add missing sets
            wagic.UnlockProfileCardSet(profile, collection.Cards.Select(x => x.Set).Distinct().ToList());
        }

        
    }
}
